<?
global $MESS;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen("/index.php"));
IncludeModuleLangFile(__FILE__);

Class noknok_kryaken extends CModule
{
        var $MODULE_ID = "noknok.kryaken";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $MODULE_DESCRIPTION;
        var $MODULE_CSS;
 
        function noknok_kryaken()
        {
            $arModuleVersion = array();

            $path = str_replace("\\", "/", __FILE__);
            $path = substr($path, 0, strlen($path) - strlen("/index.php"));
            include($path . "/version.php");

            if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
                $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            }
        
            $this->MODULE_NAME = GetMessage("MODULE_NAME");
            $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION");
            $this->PARTNER_NAME = GetMessage("MODULE_AUTHOR");
            $this->PARTNER_URI = GetMessage("MODULE_URI");
        }
		
        function DoInstall()
        {
            global $DB, $APPLICATION, $step;
            $step = IntVal($step);
            $this->InstallFiles();
            $this->InstallEvents();

            $GLOBALS["errors"] = $this->errors;
            $APPLICATION->IncludeAdminFile(GetMessage("INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/step1.php");
        }
        
        function DoUninstall()
        {
            global $DB, $APPLICATION, $step;
            $step = IntVal($step);
            $this->UnInstallEvents();
            $this->UnInstallFiles();
            $APPLICATION->IncludeAdminFile(GetMessage("UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/unstep1.php");
        }
        
        function InstallEvents()
        {
            $this->errors = false;

            RegisterModule("noknok.kryaken");
            RegisterModuleDependences("main","OnBeforeEndBufferContent", "noknok.kryaken", "noknok_kryaken_class","blockAndUp", "100");
        }
        
        function UnInstallEvents($arParams = array())
        {
            $this->errors = false;

            UnRegisterModuleDependences("main", "OnBeforeEndBufferContent", "noknok.kryaken", "noknok_kryaken_class", "blockAndUp");
            COption::RemoveOption("noknok.kryaken");
            UnRegisterModule("noknok.kryaken");

            return true;

        }

        function InstallFiles()
        {
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/noknok.kryaken/install/images", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/noknok.kryaken/", true, true);
            return true;
        }

        function UnInstallFiles()
        {
			DeleteDirFilesEx("/bitrix/images/noknok.kryaken");
			DeleteDirFilesEx("/bitrix/wizards/noknok");
			DeleteDirFilesEx("/bitrix/templates/noknok_kryaken");
            return true;
        }
}