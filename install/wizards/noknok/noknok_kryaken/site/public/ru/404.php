<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Not Found");
?>

    <div class="fhf d-flex">
      <div class="container" id="con">
        <div class="fhf-wrapper">
          <div class="paralax-img-wr" id="boxercontainer">
            <div class="first-img">
              <svg xmlns="http://www.w3.org/2000/svg" width="131.5" height="76.5">
                  <path fill="#b7bfc7" fill-rule="evenodd" stroke-width="0" d="M66.512 70.456c-34.089 2.281-62.519-7.68-63.5-22.248-.609-9.065 9.552-17.777 25.529-23.616a36.506 36.506 0 0 0-1.388 12.76c.156 2.33 1.286 5.66 5.78 8.287 3.023 1.768 7.291 3.044 12.683 3.793 4.682.65 10.172.887 15.877.685 11.919-.423 23.241-2.64 30.285-5.933 2.74-1.28 4.901-2.75 6.425-4.37 2.606-2.77 3.005-5.558 2.88-7.409a36.39 36.39 0 0 0-3.087-12.461c16.617 3.658 27.855 10.939 28.466 20.004.98 14.568-25.86 28.227-59.95 30.508zM15.69 45.917c-1.074-1.46-3.455-1.547-5.317-.194-1.861 1.352-2.5 3.632-1.426 5.092 1.074 1.46 3.455 1.547 5.317.195 1.862-1.353 2.5-3.633 1.426-5.093zm50.022 12.642c-2.53.169-4.471 1.926-4.337 3.924.135 1.998 2.294 3.481 4.823 3.312 2.53-.169 4.471-1.926 4.337-3.925-.135-1.998-2.294-3.48-4.823-3.311zm52.866-20.077c-2.026-1.092-4.374-.689-5.243.901-.869 1.59.07 3.765 2.096 4.857 2.026 1.092 4.374.689 5.243-.901.869-1.59-.069-3.765-2.096-4.857zm-28.706 1.673c-16.481 7.702-57.492 8.113-58.247-3.102a32.044 32.044 0 0 1 2.28-14.246A32.585 32.585 0 0 1 50.271 5.594 32.653 32.653 0 0 1 61.945 2.6a32.6 32.6 0 0 1 30.499 16.29 32.046 32.046 0 0 1 4.166 13.814c.197 2.93-2.434 5.439-6.738 7.451z"/>
              </svg>
            </div>

            <div class="wr-img d-flex">
              <div class="second-img">
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="109">
                    <path fill="#b7bfc7" fill-rule="evenodd" d="M21.021.566L18.897.343.149 108.047l2.124.223L21.021.566z"/>
                </svg>
              </div>
              <div class="third-img">
                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="107">
                    <path fill="#b7bfc7" fill-rule="evenodd" d="M.589.618l2.085-.39 32.195 105.691-2.085.39L.589.618z"/>
                </svg>
              </div>
            </div>
            
          </div>
          <div class="paralax-num-wr d-flex">
            <span class="paralax-num">4</span>

            <div class="num-pr-wr">
              <span class="paralax-num null-p">0</span>
            </div>
            
            <span class="paralax-num">4</span>
          </div>
          <div class="paralax-text-wr">
            <p>Извините, эта страница не найдена</p>
          </div>
          <div class="paralax-btn-wr">
            <a href="/"><button class="btn-ff" type="button">на главную</button></a>
          </div>
        </div>
      </div>
    </div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>