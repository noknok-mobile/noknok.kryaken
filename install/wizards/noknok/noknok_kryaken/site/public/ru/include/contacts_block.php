 <!-- START DUCKS MAP BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$eight_block_view = COption::GetOptionString($MODULE_ID, "eight_block_view", "color_view");?>
	<?if($eight_block_view=="color_view"):?>
		<div class="map-box-wrapper bg site-color" id="eight">
	<?elseif($eight_block_view=="image_view"):?>
		<? require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/contacts_pic.php'); ?>
	<?else:?>
		<div style="display:none;" id="eight"> 
	<?endif;?>
<?else:?>
<div class="map-box-wrapper bg site-color" id="eight">
<?endif;?>
      <div class="container">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/contacts_title.php"
				)
			);?>
        <div class="map-box-adres d-flex">
          <div class="col-md-6 col-lg-3 col-xl-3 col-12">
            <div class="map-adres-box">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_DIR."include/contacts_phone_title.php"
					)
				);?>
              <p class="map-adres-text">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/phone1.php"
				)
			);?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/phone2.php"
				)
			);?>
              </p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3 col-12">
            <div class="map-adres-box">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_email_title.php"
                    )
                );?>
              <p class="map-adres-text">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_email.php"
                    )
                );?>
            </p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3 col-12">
            <div class="map-adres-box">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_adress_title.php"
                    )
                );?>
              <p class="map-adres-text">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_adress.php"
                    )
                );?>
            </p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3 col-12">
            <div class="map-adres-box">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_schedule_title.php"
                    )
                );?>
              <p class="map-adres-text">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/contacts_schedule.php"
                    )
                );?>
            </p>
            </div>
          </div>
        </div>
          <?$APPLICATION->IncludeComponent(
			"bitrix:map.yandex.view", 
			".default", 
			array(
				"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
				),
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:47.218265541609014;s:10:\"yandex_lon\";d:39.709771268100056;s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:39.710245863754;s:3:\"LAT\";d:47.218164831616;s:4:\"TEXT\";s:74:\"г. Ростов-на-Дону, ул. Темерницкая 44, оф. 406\";}}}",
				"MAP_HEIGHT" => "377",
				"MAP_ID" => "",
				"MAP_WIDTH" => "100%",
				"OPTIONS" => array(
					0 => "ENABLE_SCROLL_ZOOM",
					1 => "ENABLE_DBLCLICK_ZOOM",
					2 => "ENABLE_DRAGGING",
				),
				"COMPONENT_TEMPLATE" => ".default"
			),
			false
		);?>
      </div>
    </div>
    <!-- END DUCKS MAP BOX -->