<p class="modal-catalog-bottom-title">Отправить заявку</p>
<p class="modal-catalog-bottom-untitle">Оставьте свой номер и мы проконсультируем вас</p>
<div class="modal-catalog-form">
	<form class="d-flex m-c-form">
		<div class="form-group">
			<input type="text" class="form-control name" name="name" placeholder="Имя" required>
		</div>
		<div class="form-group">
			<input type="tel" class="form-control tel" name="phone" placeholder="+7 (123) 456-78-90" required>
		</div>
		<button type="submit" class="btn btn-primary btn-f bg">оставьте заявку</button>
	</form>
</div>