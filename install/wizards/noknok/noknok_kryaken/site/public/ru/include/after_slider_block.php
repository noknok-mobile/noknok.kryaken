<!-- START DUCKS DESCRIPTION BOX -->
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$first_block_view = COption::GetOptionString($MODULE_ID, "first_block_view", "white_view");?>
	<?if($first_block_view=="white_view"):?>
		<div class="description-box-wrapper white-block" id="first">
	<?elseif($first_block_view=="gray_view"):?>
		<div class="description-box-wrapper gray-block" id="first">
	<?else:?>
		<div style="display:none;" id="first"> 
	<?endif;?>
<?else:?>
<div class="description-box-wrapper" id="first"> 
<?endif;?>
    <div class="container">
        <div class="description-box d-flex">
            <div class="col-md-12 col-lg-6 col-xl-6 order-desc-one">
                <div class="description-box-text">
                    
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR."include/text_block_title.php"
                            )
                        );?>
                    
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR."include/text_block_description.php"
                            )
                    );?>
                </div>
              </div>
              <div class="col-md-12 col-lg-6 col-xl-6 order-desc-two">
                <div class="description-box-img d-flex">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR."include/text_block_picture.php"
                            )
                    );?>
                </div>
              </div>
        </div>
    </div>
</div>
<!-- END DUCKS DESCRIPTION BOX -->