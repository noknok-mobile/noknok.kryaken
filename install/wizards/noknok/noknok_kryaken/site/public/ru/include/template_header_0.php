		  <header class="head" id="nnn">
			  <div class="container head-container d-flex">
				<div class="head-left d-flex">
				  <div class="head-btn-menu-box">
					<svg xmlns="http://www.w3.org/2000/svg" width="34" height="27">
						<path fill="#FFF" fill-rule="evenodd" d="M32.717 15.088H1.283C.575 15.088 0 14.377 0 13.5c0-.877.575-1.588 1.283-1.588h31.434c.708 0 1.283.711 1.283 1.588 0 .877-.575 1.588-1.283 1.588zm0-11.911H1.283C.575 3.177 0 2.465 0 1.588 0 .712.575 0 1.283 0h31.434C33.425 0 34 .712 34 1.588c0 .877-.575 1.589-1.283 1.589zM1.283 23.824h31.434c.708 0 1.283.711 1.283 1.588 0 .877-.575 1.588-1.283 1.588H1.283C.575 27 0 26.289 0 25.412c0-.877.575-1.588 1.283-1.588z"/>
					</svg>
				  </div>
				  <div class="head-logo">
					<?if(IsModuleInstalled($MODULE_ID)):?>
						<?$white_logo = COption::GetOptionString($MODULE_ID, "white_logo", "/bitrix/images/".$MODULE_ID."/white_logo.png");?>
						<?$opacity_logo = COption::GetOptionString($MODULE_ID, "opacity_logo", "/bitrix/images/".$MODULE_ID."/opacity_logo.png");?>
						<?$opacity_logo_color = COption::GetOptionString($MODULE_ID, "opacity_logo_color","Y");?>
						<img id="nl" src="<?=$white_logo;?>" alt="">
					 	<?if($opacity_logo_color == "Y"):?>
							<img id="fl" class="bg_logo" src="<?=$opacity_logo;?>" alt="">
						<?else:?>
							<img id="fl" class="" src="<?=$opacity_logo;?>" alt="">
						<?endif;?>
					<?else:?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/header_logo.php"
							)
						);?>
					<?endif;?>
				  </div>
				  <div class="head-logo-text">
					<?if(IsModuleInstalled($MODULE_ID)):?>
						<?$slogan = COption::GetOptionString($MODULE_ID, "slogan", "KRYAKEN");?>
						<?echo $slogan;?>
					<?else:?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/logo_label.php"
							)
						);?>
					<?endif;?>
				  </div>
				</div>
				<div class="head-right d-flex"> 
				  <span class="head-phone">
					<span>
					  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21">
						  <path fill="#FFF" fill-rule="evenodd" d="M19.704 16.618l-3.112-3.252c-.62-.645-1.647-.625-2.288.045l-1.568 1.638a63.944 63.944 0 0 1-.31-.18c-.99-.573-2.345-1.359-3.771-2.85-1.43-1.494-2.183-2.911-2.733-3.946-.059-.11-.114-.216-.169-.316l1.052-1.098.517-.541c.643-.672.661-1.744.042-2.39L4.252.475C3.634-.17 2.607-.151 1.964.521l-.877.921.024.025c-.294.392-.54.844-.723 1.332A5.753 5.753 0 0 0 .066 4.15c-.41 3.559 1.146 6.812 5.371 11.226 5.841 6.1 10.547 5.64 10.75 5.617a5.055 5.055 0 0 0 1.297-.341 4.997 4.997 0 0 0 1.27-.751l.02.017.888-.908c.642-.672.66-1.744.042-2.392z"/>
					  </svg>
					</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/phone1.php"
							)
						);?>
				  </span>
				  <span class="head-phone">
					<span>
					  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21">
						  <path fill="#FFF" fill-rule="evenodd" d="M19.704 16.618l-3.112-3.252c-.62-.645-1.647-.625-2.288.045l-1.568 1.638a63.944 63.944 0 0 1-.31-.18c-.99-.573-2.345-1.359-3.771-2.85-1.43-1.494-2.183-2.911-2.733-3.946-.059-.11-.114-.216-.169-.316l1.052-1.098.517-.541c.643-.672.661-1.744.042-2.39L4.252.475C3.634-.17 2.607-.151 1.964.521l-.877.921.024.025c-.294.392-.54.844-.723 1.332A5.753 5.753 0 0 0 .066 4.15c-.41 3.559 1.146 6.812 5.371 11.226 5.841 6.1 10.547 5.64 10.75 5.617a5.055 5.055 0 0 0 1.297-.341 4.997 4.997 0 0 0 1.27-.751l.02.017.888-.908c.642-.672.66-1.744.042-2.392z"/>
					  </svg>
					</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/phone2.php"
							)
						);?>
				  </span>

				  <div class="head-phone-tab">
					<span>
					  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21">
						  <path fill="#FFF" fill-rule="evenodd" d="M19.704 16.618l-3.112-3.252c-.62-.645-1.647-.625-2.288.045l-1.568 1.638a63.944 63.944 0 0 1-.31-.18c-.99-.573-2.345-1.359-3.771-2.85-1.43-1.494-2.183-2.911-2.733-3.946-.059-.11-.114-.216-.169-.316l1.052-1.098.517-.541c.643-.672.661-1.744.042-2.39L4.252.475C3.634-.17 2.607-.151 1.964.521l-.877.921.024.025c-.294.392-.54.844-.723 1.332A5.753 5.753 0 0 0 .066 4.15c-.41 3.559 1.146 6.812 5.371 11.226 5.841 6.1 10.547 5.64 10.75 5.617a5.055 5.055 0 0 0 1.297-.341 4.997 4.997 0 0 0 1.27-.751l.02.017.888-.908c.642-.672.66-1.744.042-2.392z"/>
					  </svg>
					</span>
					<div class="head-phone-tab-tel">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/phone1.php"
							)
						);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/phone2.php"
							)
						);?>
					</div>
				  </div>
		
				<button class="btn-head-right" data-toggle="modal" data-target="#modalPhone"><?=GetMessage("CALL_FEEDBACK");?></button>
		
				</div>
			  </div>
			</header>