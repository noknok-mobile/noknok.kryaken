<?
$MESS["FEEDBACK_FORM_TITLE"] = "Обратный звонок";
$MESS["FEEDBACK_FORM_DESCRIPTION"] = "Оставьте свои данные и мы бесплатно проконсультируем вас";
$MESS["FEEDBACK_FORM_NAME_PLACEHOLDER"] = "Имя";
$MESS["FEEDBACK_FORM_BUTTON"] = "оставьте заявку";
$MESS["FEEDBACK_FORM_THANKS_TITLE"] = "Спасибо!";
$MESS["FEEDBACK_FORM_THANKS_DESCRIPTION"] = "Сейчас ваша заявка попадет на почту к нашему менеджеру и он перезвонит Вам и ответит на ваши вопросы.";
$MESS["FEEDBACK_FORM_THANKS_BOTTOM_LABEL_FIRST"] = "Пока вы ожидаете звонка, посмотрите наши соц сети.";
$MESS["FEEDBACK_FORM_THANKS_BOTTOM_LABEL_SECOND"] = "Там много полезных статей";
$MESS["CONSENT_TEXT"] = "Согласен на обработку персональных данных";
?>