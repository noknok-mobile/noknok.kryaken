<?
$MESS["FEEDBACK_FORM_TITLE"] = "Callback";
$MESS["FEEDBACK_FORM_DESCRIPTION"] = "Leave your data and we will consult you for free";
$MESS["FEEDBACK_FORM_NAME_PLACEHOLDER"] = "Name";
$MESS["FEEDBACK_FORM_BUTTON"] = "Send";
$MESS["FEEDBACK_FORM_THANKS_TITLE"] = "Thanks!";
$MESS["FEEDBACK_FORM_THANKS_DESCRIPTION"] = "Now your application will be sent to our Manager and he will call you and answer your questions.";
$MESS["FEEDBACK_FORM_THANKS_BOTTOM_LABEL_FIRST"] = "While you are waiting for a call, look at our social networks.";
$MESS["FEEDBACK_FORM_THANKS_BOTTOM_LABEL_SECOND"] = "There are many useful articles";
$MESS["CONSENT_TEXT"] = "Agree";
?>