<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- START DUCKS STEPS BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$third_block_view = COption::GetOptionString($MODULE_ID, "third_block_view", "white_view");?>
	<?if($third_block_view=="white_view"):?>
		<div class="steps-box-wrapper white-block" id="third">
	<?elseif($third_block_view=="gray_view"):?>
		<div class="steps-box-wrapper gray-block" id="third">
	<?else:?>
		<div style="display:none;" id="third"> 
	<?endif;?>
<?else:?>
<div class="steps-box-wrapper" id="third"> 
<?endif;?>
    <div class="container"> 
        <p class="steps-box-title"><?=GetMessage("BLOCK_NAME6");?></p>
        <div class="steps-box d-flex">
			<?if($arParams["DISPLAY_TOP_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?>
			<?endif;?>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="col-md-6 col-lg-6 col-xl-3 step-b-w" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="step">
					  	<div class="step-img-box">
							<?if($arItem["PROPERTIES"]["ICON"]["VALUE"]):?>
								<?$arFile = CFile::GetFileArray($arItem["PROPERTIES"]["ICON"]["VALUE"]);?>
								<img src="<?=$arFile["SRC"];?>" alt="">
							<?endif;?>
						</div>
					  <div class="step-text-box">
						<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
							<p class="step-text-box-title"><?echo $arItem["NAME"]?></p>
						<?endif;?>
						<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
							<p class="step-text-box-text"><?echo $arItem["PREVIEW_TEXT"];?></p>
						<?endif;?>
					  </div>
					</div>
				</div>
			<?endforeach;?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
        </div>
    </div>
</div>
<!-- END DUCKS STEPS BOX -->