<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?function FBytes($bytes, $precision = 2) {
    $units = array('byte', 'KB', 'MB', 'GB', 'TB');
    $bytes = max($bytes, 0);
    $pow = floor(($bytes?log($bytes):0)/log(1024));
    $pow = min($pow, count($units)-1);
    $bytes /= pow(1024, $pow);
    return round($bytes, $precision).' '.$units[$pow];
}
 ?>
<!-- START DUCKS COMMENT BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$six_block_view = COption::GetOptionString($MODULE_ID, "six_block_view", "color_view");?>
	<?if($six_block_view=="color_view"):?>
		<div class="comment-box-wrapper site-color" id="six">
	<?elseif($six_block_view=="image_view"):?>
		<?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/reviews_pic.php');?>
	<?else:?>
		<div style="display:none;" id="six"> 
	<?endif;?>
<?else:?>
<div class="comment-box-wrapper site-color" id="six">
<?endif;?>
	<div class="container">
		<div class="comment-box-t-n d-flex">
			<span class="comment-box-title"><?=GetMessage("BLOCK_NAME4");?></span>
			  <div class="comment-box-nav d-flex">
				<span class="comment-box-nav-left">
				  <svg xmlns="http://www.w3.org/2000/svg" width="42" height="41">
					<path fill="#EFF2F5" fill-rule="evenodd" d="M21.413 41C10.074 41 .825 31.791.825 20.5S10.074 0 21.413 0 42 9.174 42 20.5 32.752 41 21.413 41zm0-38.107c-9.745 0-17.682 7.904-17.682 17.607s7.937 17.607 17.682 17.607c9.744 0 17.681-7.904 17.681-17.607S31.157 2.893 21.413 2.893zM18.72 30.732a1.483 1.483 0 0 1-1.028.424c-.39 0-.744-.141-1.028-.424a1.443 1.443 0 0 1 0-2.046l8.327-8.292-8.327-8.292a1.443 1.443 0 0 1 0-2.046 1.459 1.459 0 0 1 2.056 0l9.354 9.315c.284.282.425.635.425 1.023 0 .388-.141.741-.425 1.023l-9.354 9.315z"/>
				  </svg>
				</span>
				<span class="comment-box-nav-right">
				  <svg xmlns="http://www.w3.org/2000/svg" width="42" height="41">
					<path fill="#EFF2F5" fill-rule="evenodd" d="M21.413 41C10.074 41 .825 31.791.825 20.5S10.074 0 21.413 0 42 9.174 42 20.5 32.752 41 21.413 41zm0-38.107c-9.745 0-17.682 7.904-17.682 17.607s7.937 17.607 17.682 17.607c9.744 0 17.681-7.904 17.681-17.607S31.157 2.893 21.413 2.893zM18.72 30.732a1.483 1.483 0 0 1-1.028.424c-.39 0-.744-.141-1.028-.424a1.443 1.443 0 0 1 0-2.046l8.327-8.292-8.327-8.292a1.443 1.443 0 0 1 0-2.046 1.459 1.459 0 0 1 2.056 0l9.354 9.315c.284.282.425.635.425 1.023 0 .388-.141.741-.425 1.023l-9.354 9.315z"/>
				  </svg>
				</span>
			  </div>
        </div>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
        <div class="comment-box-slider-wrapper">
			<div class="comment-box-slider">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
						<div class="comment-box-slide-wr" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						  <div class="comment-box-slide">
							<div class="comment-box-slide-img">
								<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
								<?endif;?>
							</div>
							<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
								<div class="comment-box-slide-title">
								  <p><?=$arItem["NAME"];?></p>
								</div>
							<?endif;?>
							<div class="comment-box-slide-text">
								<p>
									<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
										<?echo $arItem["PREVIEW_TEXT"];?>
									<?endif;?>
								</p>
							</div>
							<div class="comment-box-slide-file d-flex">
							  <?if($arItem["PROPERTIES"]["PDF"]["VALUE"]):?>
								  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24">
									  <path fill="#0076E3" fill-rule="evenodd" d="M17.386 24H3.924c-1.442 0-2.615-1.146-2.615-2.555v-1.144h-.255c-.582 0-1.054-.46-1.054-1.029v-6.243C0 12.461.472 12 1.054 12h.255V2.554C1.309 1.147 2.482 0 3.924 0h9.866L20 6.045v15.4C20 22.854 18.827 24 17.386 24zM2.764 18.428h.993V16.88c.092.012.21.019.342.019.591 0 1.097-.141 1.439-.456.263-.244.408-.605.408-1.028 0-.423-.191-.784-.474-1.002-.296-.231-.736-.347-1.354-.347-.612 0-1.045.039-1.354.09v4.272zM18.36 6.665l-3.356.01c-1.001 0-1.813-.794-1.813-1.772V1.557l-9.267.009c-.559 0-1.013.444-1.013.988V12h12.55c.582 0 1.054.461 1.054 1.029v6.243c0 .569-.472 1.029-1.054 1.029H2.911v1.144c0 .546.454.991 1.013.991h13.462c.558 0 1.012-.445 1.012-.991l-.038-14.78zM6.586 14.156v4.252c.25.032.618.064 1.137.064.868 0 1.578-.179 2.045-.558.428-.354.737-.925.737-1.754 0-.765-.29-1.297-.75-1.631-.427-.315-.973-.463-1.815-.463-.506 0-.986.033-1.354.09zm7.175 2.531v-.797H12.17v-.989h1.703v-.802h-2.709v4.329h1.006v-1.741h1.591zM7.94 17.708c-.124 0-.263 0-.348-.019v-2.832c.085-.02.23-.039.453-.039.861 0 1.401.476 1.395 1.375 0 1.034-.592 1.521-1.5 1.515zm-2.987-2.267c0 .443-.328.706-.861.706-.145 0-.25-.007-.335-.025v-1.279c.072-.018.21-.038.414-.038.499 0 .782.237.782.636z"/>
								  </svg>
									<?$arFile = CFile::GetFileArray($arItem["PROPERTIES"]["PDF"]["VALUE"]);?>
									<a target="_blank" href="<?=$arFile["SRC"]?>"><?=$arFile["ORIGINAL_NAME"]?> (<? echo FBytes($arFile["FILE_SIZE"]);?>)</a>
								<?endif;?>
							</div>
						  </div>
						</div>
				<?endforeach;?>
			</div>
		</div>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
	</div>
</div>
<!-- END DUCKS COMMENT BOX -->