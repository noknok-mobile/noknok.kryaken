if (!window.BX_YMapAddPlacemark)
{



	window.BX_YMapAddPlacemark = function(map, arPlacemark)
	{

 MyIconLayout = ymaps.templateLayoutFactory.createClass([
  	'<div class="icon-map">',
            '<svg xmlns="http://www.w3.org/2000/svg" width="38" height="51">',
                '<path fill="' + color + '" fill-rule="evenodd" d="M19 0C8.523 0 0 8.281 0 18.46c0 12.632 17.003 31.176 17.727 31.96.68.735 1.867.734 2.546 0C20.997 49.636 38 31.092 38 18.46 38 8.281 29.477 0 19 0zm0 27.748c-5.271 0-9.559-4.167-9.559-9.288 0-5.122 4.288-9.288 9.559-9.288 5.271 0 9.559 4.167 9.559 9.288S24.271 27.748 19 27.748z"/>',
            '</svg>',
    '</div>'
        ].join(''));

		if (null == map)
			return false;

		if(!arPlacemark.LAT || !arPlacemark.LON)
			return false;

		var props = {};
		if (null != arPlacemark.TEXT && arPlacemark.TEXT.length > 0)
		{
			var value_view = '';

			if (arPlacemark.TEXT.length > 0)
			{
				var rnpos = arPlacemark.TEXT.indexOf("\n");
				value_view = rnpos <= 0 ? arPlacemark.TEXT : arPlacemark.TEXT.substring(0, rnpos);
			}

			props.balloonContent = arPlacemark.TEXT.replace(/\n/g, '<br />');
			props.hintContent = value_view;
		}

		var obPlacemark = new ymaps.Placemark(
			[arPlacemark.LAT, arPlacemark.LON],
			props,
			{balloonCloseButton: true,
			iconLayout: MyIconLayout,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-19, -55], [19, 0]
                ]
			}
			});

		map.geoObjects.add(obPlacemark);

		return obPlacemark;
	}
}

if (!window.BX_YMapAddPolyline)
{
	window.BX_YMapAddPolyline = function(map, arPolyline)
	{
		if (null == map)
			return false;

		if (null != arPolyline.POINTS && arPolyline.POINTS.length > 1)
		{
			var arPoints = [];
			for (var i = 0, len = arPolyline.POINTS.length; i < len; i++)
			{
				arPoints.push([arPolyline.POINTS[i].LAT, arPolyline.POINTS[i].LON]);
			}
		}
		else
		{
			return false;
		}

		var obParams = {clickable: true};
		if (null != arPolyline.STYLE)
		{
			obParams.strokeColor = arPolyline.STYLE.strokeColor;
			obParams.strokeWidth = arPolyline.STYLE.strokeWidth;
		}
		var obPolyline = new ymaps.Polyline(
			arPoints, {balloonContent: arPolyline.TITLE}, obParams
		);

		map.geoObjects.add(obPolyline);

		return obPolyline;
	}
}