<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- START DUCKS GALLERY BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$seven_block_view = COption::GetOptionString($MODULE_ID, "seven_block_view", "white_view");?>
	<?if($seven_block_view=="white_view"):?>
		<div class="gallery-box-wrapper white-block" id="seven">
	<?elseif($seven_block_view=="gray_view"):?>
		<div class="gallery-box-wrapper gray-block" id="seven">
	<?else:?>
		<div style="display:none;" id="seven"> 
	<?endif;?>
<?else:?>
<div class="gallery-box-wrapper" id="seven">
<?endif;?>
		<div class="container">
			<div class="catalog-box-t-f-wrapper d-flex">
			  <span class="catalog-box-title"><?=GetMessage("BLOCK_NAME3");?></span>
			  <div class="gallery-box-f">
				  <?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/gallery_tags.php');?>
			  </div>
			</div>
			<div class="gallery-box d-flex filtr-gallery">
				<?if($arParams["DISPLAY_TOP_PAGER"]):?>
					<?=$arResult["NAV_STRING"]?>
				<?endif;?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<?
						$db_old_groups = CIBlockElement::GetElementGroups($arItem["ID"], true);
						$IDS = "";
						$NAME = "";
						while($ar_group = $db_old_groups->Fetch())
						{
						
							$IDS.= $ar_group["ID"].",";
							$NAME.= $ar_group["NAME"].",";
						}
						$item_info[$arItem["ID"]] = array($IDS,$NAME);
					?>
				  <div class="gallery-out filtr-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-category="<?echo $item_info[$arItem["ID"]][0];?>">
					<div class="gallery-div">
					  <a data-fancybox="gallery" href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a>
					</div>
				  </div>
				<?endforeach;?>
				<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
					<br /><?=$arResult["NAV_STRING"]?>
				<?endif;?>
			</div>
		</div>
	</div>
<!-- END DUCKS GALLERY BOX -->