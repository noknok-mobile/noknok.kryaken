<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- START DUCKS CATALOG BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$five_block_view = COption::GetOptionString($MODULE_ID, "five_block_view", "white_view");?>
	<?if($five_block_view=="white_view"):?>
		<div class="catalog-box-wrapper white-block" id="five">
	<?elseif($five_block_view=="gray_view"):?>
		<div class="catalog-box-wrapper gray-block" id="five">
	<?else:?>
		<div style="display:none;" id="five"> 
	<?endif;?>
<?else:?>
<div class="catalog-box-wrapper" id="five">
<?endif;?>
	<div class="container">
		<div class="catalog-box-t-f-wrapper d-flex">
			  <span class="catalog-box-title"><?=GetMessage("BLOCK_NAME2");?></span>
			  <div class="catalog-box-f">
				  <?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/catalog_tags.php');?>
			  </div>
        </div>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
		   <!--start каталог модальное окно-->
				<!-- Modal phone -->
				<div class="modal modal-catalog fade" id="<?=$arItem["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
					  <div class="modal-body">
						<div class="modal-cl-box">
						  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						<div class="modal-catalog-wrapper-top d-flex">
						  <div class="modal-catalog-left">
							<div class="modal-catalog-slider-wr">
							  <div class="modal-catalog-slider">
								<?if (!empty($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"])):?>
									<?foreach ($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):
									$URL = CFile::GetPath($pic);
									?>
									<div class="modal-catalog-slide">
									  <div class="modal-catalog-slide-in d-flex">
										<img src="<?=$URL?>" alt="">
									  </div>
									</div>
									<?endforeach;?>
								<?endif;?>
							  </div>
							  <div class="modal-catalog-slider-nav">
								<?if (!empty($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"])):?>
									<?foreach ($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):
									$URL2 = CFile::GetPath($pic);
									?>
										<div class="modal-catalog-slide-nav">
										  <div class="modal-catalog-slide-nav-in">
											<img src="<?=$URL2;?>" alt="">
										  </div>
										</div>
									<?endforeach;?>
								<?endif;?>
							  </div>
							</div>
						  </div>
						  <div class="modal-catalog-right">
							<div class="modal-catalog-desc">
								<p class="modal-catalog-desc-title"><?=$arItem["NAME"];?></p>
								<?if($arItem["DETAIL_TEXT"]):?>
									<?echo $arItem["DETAIL_TEXT"];?>
								<?endif;?>
							  <div class="modal-catalog-desc-price">
								<?if ($arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"]!="" && $arItem["PROPERTIES"]["PRICE"]["VALUE"]!=""):?>
									<span class="modal-old-price"><del><?=$arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"];?></del></span>
									<span class="modal-price"><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"];?></span>
								<?elseif ($arItem["PROPERTIES"]["PRICE"]["VALUE"]!=""):?>
									<span class="modal-price"><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"];?></span>
								<?endif;?>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="modal-catalog-wrapper-bottom">
							<?require($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/form.php');?>
					  </div>
					</div>
				  </div>
				</div>
			<!--end каталог модальное окно-->
		<?endforeach;?>

		<div class="catalog-box-filtr-container-wr">
			<div class="catalog-box d-flex filtr-container">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<?
					$db_old_groups = CIBlockElement::GetElementGroups($arItem["ID"], true);
					$IDS = "";
					$NAME = "";
					while($ar_group = $db_old_groups->Fetch())
					{
					
						$IDS.= $ar_group["ID"].",";
						$NAME.= $ar_group["NAME"].",";
					}
					$item_info[$arItem["ID"]] = array($IDS,$NAME);
					?>


					<div class="filtr-item catalog-out" data-category="<?echo $item_info[$arItem["ID"]][0];?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="catalog-div">
							<div class="catalog-div-img <?echo $arItem["PROPERTIES"]["OFFER"]["VALUE_XML_ID"];?> d-flex">
								<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
								<?endif;?>
							</div>
							<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
								<p class="catalog-div-title"><?=$arItem["NAME"];?></p>
							<?endif;?>
							<div class="catalog-div-price">
								<?if ($arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"]!="" && $arItem["PROPERTIES"]["PRICE"]["VALUE"]!=""):?>
									<span class="catalog-div-price-old"><del><?=$arItem["PROPERTIES"]["OLD_PRICE"]["VALUE"];?></del></span>
									<span class="catalog-div-price-new"><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"];?></span>
								<?elseif ($arItem["PROPERTIES"]["PRICE"]["VALUE"]!=""):?>
									<span class="catalog-div-price-new"><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"];?></span>
								<?else:?>
									<span class="catalog-div-price-new"></span>
								<?endif;?>
							</div>
							<button class="catalog-div-btn" data-toggle="modal" data-target="#<?=$arItem["ID"];?>"><?=GetMessage("MORE_INFO");?></button>
						</div>
					</div>
				<?endforeach;?>
			</div>
      </div>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>
	</div>
</div>
<!-- END DUCKS CATALOG BOX -->