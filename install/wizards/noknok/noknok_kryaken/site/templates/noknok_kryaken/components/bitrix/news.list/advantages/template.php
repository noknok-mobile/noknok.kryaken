<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- START DUCKS GOODS BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$fourth_block_view = COption::GetOptionString($MODULE_ID, "fourth_block_view", "color_view");?>
	<?if($fourth_block_view=="color_view"):?>
		<div class="goods-box-wrapper site-color" id="fourth">
	<?elseif($fourth_block_view=="image_view"):?>
		<?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/advantages_pic.php');?>
	<?else:?>
		<div style="display:none;" id="fourth"> 
	<?endif;?>
<?else:?>
<div class="goods-box-wrapper site-color" id="fourth">
<?endif;?>
    <div class="container">
        <p class="goods-box-title"><?=GetMessage("BLOCK_NAME");?></p>
        <div class="goods-box d-flex">
			<?if($arParams["DISPLAY_TOP_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?>
			<?endif;?>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="col-md-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="goods">
						<div class="goods-t-i-box">
							<?if($arItem["PROPERTIES"]["ICON"]["VALUE"]):?>
								<?$arFile = CFile::GetFileArray($arItem["PROPERTIES"]["ICON"]["VALUE"]);?>
								<img src="<?=$arFile["SRC"];?>" alt="" alt="" class="goods-i">
							<?endif;?>
							<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
								<span class="goods-t"><?echo $arItem["NAME"]?></span>
							<?endif;?>
						</div>
						<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
						  <p class="goods-text"><?echo $arItem["PREVIEW_TEXT"];?></p>
						<?endif;?>
					</div>
				</div>
			<?endforeach;?>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?>
			<?endif;?>
        </div>
    </div>
</div>
<!-- END DUCKS GOODS BOX -->