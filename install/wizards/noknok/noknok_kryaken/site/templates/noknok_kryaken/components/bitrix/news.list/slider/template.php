<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="hero-box" id="up-first"> 
    <div class="slider-hero">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="hero-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>" <?if($arItem["PROPERTIES"]["IS_VIDEO"]["VALUE"]!="Y" && is_array($arItem["DETAIL_PICTURE"]) ):?>style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"<?endif;?>>
			<?/*если видео-слайд*/?>
			<?if($arItem["PROPERTIES"]["IS_VIDEO"]["VALUE"]=="Y"):?>
				 <div class="video-bg">
					<video preload="auto" autoplay="autoplay" loop="loop" width="100%" height="auto" muted onloadedmetadata="this.muted = true">
						<?if($arItem["DISPLAY_PROPERTIES"]["VIDEO"]["FILE_VALUE"]["SRC"]):?>
							<source src="<?=$arItem["DISPLAY_PROPERTIES"]["VIDEO"]["FILE_VALUE"]["SRC"]?>" type="video/mp4">
						<?else:?>
							<source src="<?=SITE_TEMPLATE_PATH?>/img/1.mp4" type="video/mp4">
						<?endif;?>
					</video>
				</div>
				<div class="container hero-slide-inner d-flex">
					<div class="col-12">
						<div class="hero-slide-inner-text-wr d-flex t-c">
							<div class="hero-slide-inner-text-center">
								<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<p class="hero-slide-inner-title"><?=$arItem["NAME"];?></p>
								<?endif;?>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<p class="hero-slide-inner-text"><?echo $arItem["PREVIEW_TEXT"];?></p>
								<?endif;?>
								<?if($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["SHOW_FORM"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"]):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?endif;?>
							</div>
						</div>
					</div>
				</div>
			<?/*если слайд с наложенной картинкой*/?>
			<?elseif($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<div class="container hero-slide-inner d-flex">
					<div class="col-xl-7 col-lg-12 col-md-12 col-12">
						<div class="hero-slide-inner-text-wr d-flex">
							<div class="hero-slide-inner-text-center">
								<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<p class="hero-slide-inner-title"><?=$arItem["NAME"];?></p>
								<?endif;?>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<p class="hero-slide-inner-text"><?echo $arItem["PREVIEW_TEXT"];?></p>
								<?endif;?>
								<?if($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["SHOW_FORM"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"]):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?endif;?>
							</div>
						</div>
					</div>
					<div class="col-xl-5 hidden-lg">
						<div class="hero-slide-inner-img-wr d-flex">
							<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
						</div>
					</div>
				</div>
			<?/*если простой слайд с фоном*/?>
			<?else:?>
				<div class="container hero-slide-inner d-flex">
					<div class="col-12">
						<div class="hero-slide-inner-text-wr d-flex t-c">
							<div class="hero-slide-inner-text-center">
								<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<p class="hero-slide-inner-title"><?=$arItem["NAME"];?></p>
								<?endif;?>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<p class="hero-slide-inner-text"><?echo $arItem["PREVIEW_TEXT"];?></p>
								<?endif;?>
								<?if($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["SHOW_FORM"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button" data-toggle="modal" data-target="#modalPhone"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y" && $arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"]):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"];?>" target="_blank"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?elseif($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y"):?>
									<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
									<?else:?>
										<a href="#!"><button class="hero-btn bg" type="button"><?=GetMessage("BUTTON_LABEL");?></button></a>
									<?endif;?>
								<?endif;?>
							</div>
						</div>
					</div>
				</div>
			<?endif;?>
		</div>
	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	</div>
</div>
<script>
$('.slider-hero').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: false,
  cssEase: 'ease',
autoplay: true,
  autoplaySpeed: 5000,
});

if ($(window).width() > 768) {
  $('.hero-slide').height($(window).height());
} else {
$('.hero-slide').height(765);
}
</script>
<!-- END HERO BOX -->