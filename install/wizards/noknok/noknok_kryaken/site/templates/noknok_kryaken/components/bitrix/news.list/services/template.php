<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- START DUCKS SERVICE BOX -->
<?$MODULE_ID = "noknok.kryaken";?>
<?CModule::IncludeModule($MODULE_ID);?>
<?if(IsModuleInstalled($MODULE_ID)):?>
	<?$second_block_view = COption::GetOptionString($MODULE_ID, "second_block_view", "gray_view");?>
	<?if($second_block_view=="white_view"):?>
		<div class="service-wrapper-box white-block" id="second">
	<?elseif($second_block_view=="gray_view"):?>
		<div class="service-wrapper-box gray-block" id="second">
	<?else:?>
		<div style="display:none;" id="second"> 
	<?endif;?>
<?else:?>
<div class="service-wrapper-box" id="second">
<?endif;?>
    <div class="container">
        <p class="service-box-title"><?=GetMessage("BLOCK_NAME5");?></p>
            <div class="service-box-wr d-flex">
				<?if($arParams["DISPLAY_TOP_PAGER"]):?>
					<?=$arResult["NAV_STRING"]?>
				<?endif;?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<div class="col-md-12 col-xl-6 col-lg-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="service-box d-flex">
							<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
								<div class="service-box-img" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>);">
							<?else:?>
								<div class="service-box-img">
							<?endif;?>
									<div class="service-box-img-bg"></div>
									<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
										<p class="service-box-img-title"><?echo $arItem["NAME"]?></p>
									<?endif;?>
								</div>
								<div class="service-box-text d-flex">
									<p>
										<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
											<?echo $arItem["PREVIEW_TEXT"];?>
										<?endif;?>
									</p>
									<div class="service-box-text-bottom d-flex">
										<a href="#!" class="service-box-link" data-toggle="modal" data-target=".<?=$arItem["ID"];?>"><?=GetMessage("MORE");?></a>
										<?if($arItem["PROPERTIES"]["USE_BUTTON"]["VALUE"]=="Y"):?>
											<?if($arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"]):?>
												<a href="#!"><button class="s-btn" type="button" data-toggle="modal" data-target="#modalPhone" tabindex="0"><?=$arItem["PROPERTIES"]["BUTTON_LABEL"]["VALUE"];?></button></a>
											<?else:?>
												<a href="#!"><button class="s-btn" type="button" data-toggle="modal" data-target="#modalPhone" tabindex="0"><?=GetMessage("BUTTON_LABEL");?></button></a>
											<?endif;?>
										<?endif;?>
									</div>
								</div>
								<!--start УСЛУГИ модальное окно-->
								<!-- Modal phone -->
								<div class="modal modal-service fade <?=$arItem["ID"];?>" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-body">
												<div class="modal-cl-box">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["DETAIL_PICTURE"])):?>
													<div class="service-modal-header" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);">
												<?else:?>
													<div class="service-modal-header">
												<?endif;?>
														<div class="service-modal-header-bg"></div>
														<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
															<p class="service-modal-title"><?echo $arItem["NAME"]?></p>
														<?endif;?>
													</div>
													<?if($arItem["DETAIL_TEXT"]):?>
														<div class="service-modal-box-wrapper">
															<?echo $arItem["DETAIL_TEXT"];?>
														</div>
													<?endif;?>
											</div>
											<div class="modal-catalog-wrapper-bottom">
												<?require($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/form.php');?>
											</div>
										</div>
									</div>
								</div>
								<!--end УСЛУГИ модальное окно-->
						</div>
					</div>
				<?endforeach;?>
				<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
					<?=$arResult["NAV_STRING"]?>
				<?endif;?>
        	</div>
    </div>
</div>
<!-- END DUCKS SERVICE BOX -->