<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$MODULE_ID = "noknok.kryaken";

if(isset($_POST["color"]) && isset($_POST["hover"]) && isset($_POST["opacity"]) && IsModuleInstalled($MODULE_ID))
{
	COption::SetOptionString($MODULE_ID, "main_color", $_POST["color"]);
	COption::SetOptionString($MODULE_ID, "hover_color", $_POST["hover"]);
	COption::SetOptionString($MODULE_ID, "opacity_color", $_POST["opacity"]);
}
if(isset($_POST["header"]) && IsModuleInstalled($MODULE_ID))
{
	COption::SetOptionString($MODULE_ID, "header", $_POST["header"]);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>