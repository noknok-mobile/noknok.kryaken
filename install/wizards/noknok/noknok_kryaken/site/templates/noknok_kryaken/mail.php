<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
IncludeTemplateLangFile(__FILE__);
$call_title = GetMessage("CALL_TITLE");
if($_POST["form"]==$call_title || $_POST["form"]=="Callback")
{  
$arEventFields = array(
    "NAME"                  => $_POST["name"],
    "PHONE"             => $_POST["phone"]
    );
CEvent::SendImmediate("NOKNOK_FEEDBACK_FORM", "s1", $arEventFields); 
}
else
{ 
 $arEventFields = array(
    "NAME"                  => $_POST["name"],
    "PHONE"             => $_POST["phone"],
    "ITEM" => $_POST["form"]
    );
CEvent::SendImmediate("NOKNOK_SERVICES_FEEDBACK", "s1", $arEventFields);   
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>