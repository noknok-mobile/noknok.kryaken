function set_cookie ( name, value, exp_y, exp_m, exp_d, path, domain, secure )
{
  var cookie_string = name + "=" + escape ( value );
 
  if ( exp_y )
  {
    var expires = new Date ( exp_y, exp_m, exp_d );
    cookie_string += "; expires=" + expires.toGMTString();
  }
 
  if ( path )
        cookie_string += "; path=" + escape ( path );
 
  if ( domain )
        cookie_string += "; domain=" + escape ( domain );
  
  if ( secure )
        cookie_string += "; secure";
  
  document.cookie = cookie_string;
}
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}
function delete_cookie ( cookie_name )
{
  var cookie_date = new Date ( );  // Текущая дата и время
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

//BACKGROUND COOKIE
if ( ! get_cookie ( "colorpick" ) )
{

  } else
{
  delete_cookie ( "colorpick_block" );
  colorpick_cook = get_cookie ( "colorpick" );
  $('.bg').css('background-color', colorpick_cook);
  $('.hero-btn').css('border-color', colorpick_cook);
  
  $('.icon-map svg path').css('fill', colorpick_cook);
  $('.btn-f').css('border-color', colorpick_cook);
  $('.comment-box-slide-file svg path').css('fill', colorpick_cook);

  
}
//HOVER COOKIE
if ( ! get_cookie ( "hoverrpick" ) )
{

  } else
{
  colorhover_cook = get_cookie ( "hoverrpick" );
  $('#hsl').css('background-color', colorhover_cook);
  rgb = colorhover_cook;
}
;
//HAND COOKIE
if ( ! get_cookie ( "colorpick_block" ) )
{

  } else
{
  var colorhand_cook = get_cookie ( "colorpick_block" );
  $('.bg').css('background-color', colorhand_cook);
  $('.hero-btn').css('border-color', colorhand_cook);
  $('.btn-f').css('border-color', colorhand_cook);
  $('.comment-box-slide-file svg path').css('fill', colorhand_cook);
  $('.icon-map svg path').css('fill', colorhand_cook);
};
//ПЕРЕВОД ЦВЕТОВ
function toHEX(r, g, b) {
  return '#' + ((b | g << 8 | r << 16) | 1 << 24).toString(16).slice(1);
}
function getHexRGBColor(color)
    {
      color = color.replace(/\s/g,"");
      var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
     
      if(aRGB)

      {

        color = '';

        for (var i=1;  i<=3; i++) color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i])).toString(16).replace(/^(.)$/,'0$1');

      }

      else color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
 
      return color;
    }
/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
  r /= 255, g /= 255, b /= 255;
  var max = Math.max(r, g, b),
    min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;

  if (max == min) {
    h = s = 0; // achromatic
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h /= 6;
  }

  return [h, s, l];
}


function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    var hue2rgb = function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}
//ПЕРЕВОД ЦВЕТОВ


$(document).ready(function() {

/*ручная смена*/
$('.pick-block-color').click(function(){
    var pick_block = $(this).css('background-color');
    
    set_cookie ( "colorpick_block", pick_block );
    $('.bg').css('background-color', pick_block);
    $('.comment-box-slide-file svg path').css('fill', pick_block);
    $('.icon-map svg path').css('fill', pick_block);
    $('.hero-btn').css('border-color', pick_block);
    $('.btn-f').css('border-color', pick_block);
    $('.s-btn').css({"border": 'solid 1px ' + pick_block + ''});
    $('.s-btn').css({"color": pick_block});
    $('.catalog-div-btn').css({"border": 'solid 1px ' + pick_block + ''});
    $('.catalog-div-btn').css({"color": pick_block});
    $(".color-demo").css({"color": pick_block});
    $("#fl").css({
      "background-color": pick_block
    });
    $('.color_test').css({
      "color": pick_block
    });
    delete_cookie ( "colorpick" );

    var c = getHexRGBColor(pick_block);

    //HEX to RGB
    var r = parseInt(c.substring(0, 2), 16);
    var g = parseInt(c.substring(2, 4), 16);
    var b = parseInt(c.substring(4, 6), 16);
    

    $("#rgb").css({
      "background": c
    });

    //RGB to HSL
    var hsl = rgbToHsl(r, g, b);


    var h = parseFloat(hsl[0]);
    var s = parseFloat(hsl[1]);
    var l = parseFloat(hsl[2]);

    //set second color
    l = l + 0.20;
    s = s - 0.04;
    h = h + 0;

    //HSL to RGB
    rgb = hslToRgb(h, s, l);

    //RGB to HEX
    rgb = toHEX(rgb[0], rgb[1], rgb[2]);
    set_cookie ( "hoverrpick", rgb );
    $("#hsl").css({
      "background-color": rgb
    });


    $("#gr").css({
      "background": "linear-gradient(to top, " + rgb + " , " + c + ")"
    });
});
/*ручная смена*/

var for_pick_color = $('.bg').css('background-color');
var newColor;
function colorChange(color) {
  newColor = color.toHexString();
  set_cookie ( "colorpick", newColor );
  $('.bg').css('background-color', newColor);
  $('.comment-box-slide-file svg path').css('fill', newColor);
  $('.icon-map svg path').css('fill', newColor);
  $('.hero-btn').css('border-color', newColor);
  $('.btn-f').css('border-color', newColor);
  $('.s-btn').css({"border": 'solid 1px ' + newColor + ''});
  $('.s-btn').css({"color": newColor});
  
  $('.catalog-div-btn').css({"border": 'solid 1px ' + newColor + ''});
  $('.catalog-div-btn').css({"color": newColor});
  $(".color-demo").css({"color": newColor});
$("#fl").css({
      "background-color": newColor
    });
    $('.color_test').css({
      "color": newColor
    });
}
$(".colorswitch").spectrum({
    color: for_pick_color,
    containerClassName: 'sp-bg',
    showInitial: true,
    chooseText: "Выбрать",
    cancelText: "Отмена",
    showInput: true,
    preferredFormat: "hex",
    move: function(color) {
      colorChange(color);

var c = color.toHexString();

    //HEX to RGB
    var r = parseInt(c.substring(1, 3), 16);
    var g = parseInt(c.substring(3, 5), 16);
    var b = parseInt(c.substring(5, 7), 16);

    $("#rgb").css({
      "background": c
    });

    //RGB to HSL
    var hsl = rgbToHsl(r, g, b);


    var h = parseFloat(hsl[0]);
    var s = parseFloat(hsl[1]);
    var l = parseFloat(hsl[2]);

    //set second color
    l = l + 0.20;
    s = s - 0.04;
    h = h + 0;

    //HSL to RGB
     rgb = hslToRgb(h, s, l);

    //RGB to HEX
     rgb = toHEX(rgb[0], rgb[1], rgb[2]);
    set_cookie ( "hoverrpick", rgb );
    $("#hsl").css({
      "background-color": rgb
    });

    $("#gr").css({
      "background": "linear-gradient(to top, " + rgb + " , " + c + ")"
    });

    },
    change: function(color) {
      colorChange(color);
          var c = color.toHexString();

    //HEX to RGB
    var r = parseInt(c.substring(1, 3), 16);
    var g = parseInt(c.substring(3, 5), 16);
    var b = parseInt(c.substring(5, 7), 16);

    $("#rgb").css({
      "background": c
    });

    //RGB to HSL
    var hsl = rgbToHsl(r, g, b);


    var h = parseFloat(hsl[0]);
    var s = parseFloat(hsl[1]);
    var l = parseFloat(hsl[2]);

    //set second color
    l = l + 0.20;
    s = s - 0.04;
    h = h + 0;

    //HSL to RGB
     rgb = hslToRgb(h, s, l);

    //RGB to HEX
     rgb = toHEX(rgb[0], rgb[1], rgb[2]);
    set_cookie ( "hoverrpick", rgb );
    $("#hsl").css({
      "background-color": rgb
    });

    $("#gr").css({
      "background": "linear-gradient(to top, " + rgb + " , " + c + ")"
    });
    }
});




});

/*В ЭТОМ ВИДЕ ТОЛЬКО ДЛЯ ДЕМО !!!*/

function color_header() {
var div = document.getElementById('nnn');

$('.catalog-box-f span').css({"color": ''});
$('.catalog-box-f span.active').css({"color": $('.bg').css('background-color')});

$('.gallery-box-f span').css({"color": ''});
$('.gallery-box-f span.active').css({"color": $('.bg').css('background-color')});

$('.slick-dots li button').css({"background-color": ''});
$('.f-bg button').css({"background-color": $('.bg').css('background-color')});


if( div.classList.contains('fixed-on'))
  {
    $('.color_test').css({"color": $('.bg').css('background-color')});
    $('#fl').css({"background-color": $('.bg').css('background-color')});
    $('.head-btn-menu-box svg path').css({"fill": $('.bg').css('background-color')});
    $('.head-phone span').css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
    $('.head-phone svg path').css({"fill": $('.bg').css('background-color')});
    $('.head .btn-head-right').css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
    $('.head .btn-head-right').css({"color": $('.bg').css('background-color')});


    $('#nl').fadeOut( 300, function() {
    $('#fl').fadeIn(200);
  });
  } else {
    $('.color_test').css({"color": ''});
    $('.head-btn-menu-box svg path').css({"fill": ''});
    $('.head-phone span').css({"border": ''});
    $('.head-phone svg path').css({"fill": ''});
    $('.head .btn-head-right').css({"border": ''});
    $('.head .btn-head-right').css({"color": ''});
    
    
    $('#fl').fadeOut(50, function() {
    $('#nl').fadeIn(50);
  });
  }
}

timerId = setInterval(color_header, 1);

(function($) {
  $(document).ready(function() {



jQuery('.catalog-div').on({
    mouseenter: function() {
        $(this).find('.catalog-div-price-new').css({"color": $('.bg').css('background-color')});
    },
    mouseleave: function() {
      $(this).find('.catalog-div-price-new').css({"color": ''});
    }
});

jQuery('.hero-btn').on({
    mouseenter: function() {
        $(this).css({"border": 'solid 1px ' + rgb + ''});
        $(this).css({"background-color":  rgb });
    },
    mouseleave: function() {
      $(this).css({"background-color": $('.bg').css('background-color')});
      $(this).css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
    }
});
jQuery('.btn-f').on({
    mouseenter: function() {
        $(this).css({"border": 'solid 1px ' + rgb + ''});
        $(this).css({"background-color":  rgb });
    },
    mouseleave: function() {
      $(this).css({"background-color": $('.bg').css('background-color')});
      $(this).css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
    }
});



jQuery('.head .head-phone').on({
    mouseenter: function() {
      clearInterval(timerId);
        $(this).find("span").css({"border": 'solid 1px ' + rgb + ''});
        $(this).find('svg path').css({"fill": rgb});
        $(this).find("a").css({"color": rgb});
    },
    mouseleave: function() {
      timerId = setInterval(color_header, 1);
    }
});

jQuery('.ctlg-fltr').on({
    mouseenter: function() {
      clearInterval(timerId);
        $(this).css({"color": $('.bg').css('background-color')});
    },
    mouseleave: function() {
$(this).css({"color": ''});
      timerId = setInterval(color_header, 1);
    }
});

jQuery('.gal-fltr').on({
    mouseenter: function() {
      clearInterval(timerId);
        $(this).css({"color": $('.bg').css('background-color')});
    },
    mouseleave: function() {
$(this).css({"color": ''});
      timerId = setInterval(color_header, 1);
    }
});


jQuery('.head .head-btn-menu-box').on({
    mouseenter: function() {
      clearInterval(timerId);
        
        $(this).find('svg path').css({"fill": $('.bg').css('background-color')});
        
    },
    mouseleave: function() {
      timerId = setInterval(color_header, 1);
    }
});

/*HOVER FOR DEMO*/

$( ".head.fixed-on .btn-head-right" ).hover(function() {
  $( this ).css({"background-color": $('.bg').css('background-color')});

    }, function() {
    $( this ).css({"background-color": ''});
  });

$( ".head .btn-head-right" ).hover(function() {
    $( this ).css({"background-color": $('.bg').css('background-color')});
    $(this).addClass('border-tr');
    }, function() {
    $( this ).css({"background-color": ''});
    $(this).removeClass('border-tr');
  });
/*
$( ".head .btn-head-right" ).hover(function() {
    $( this ).css({"background-color": $('.bg').css('background-color')});
    $(this).addClass('border-tr');
    }, function() {
    $( this ).css({"background-color": ''});
    $(this).removeClass('border-tr');
  });
*/
/*HOVER FOR DEMO*/

$('.s-btn').css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
$('.s-btn').css({"color": $('.bg').css('background-color')});
$('.color-demo').css({"color": $('.bg').css('background-color')});
$( ".s-btn" ).hover(function() {
    $( this ).css({"background-color": $('.bg').css('background-color')});
    $(this).addClass('border-tr');
    }, function() {
    $( this ).css({"background-color": ''});
    $(this).removeClass('border-tr');
  });

$('.catalog-div-btn').css({"border": 'solid 1px ' + $('.bg').css('background-color') + ''});
$('.catalog-div-btn').css({"color": $('.bg').css('background-color')});
$('.color-demo').css({"color": $('.bg').css('background-color')});
$( ".catalog-div-btn" ).hover(function() {
    $( this ).css({"background-color": $('.bg').css('background-color')});
    $(this).addClass('border-tr');
    }, function() {
    $( this ).css({"background-color": ''});
    $(this).removeClass('border-tr');
  });


/*HOVER FOR DEMO*/




/*HOVER FOR DEMO*/

/*В ЭТОМ ВИДЕ ТОЛЬКО ДЛЯ ДЕМО !!!*/


    
  });
})(jQuery)