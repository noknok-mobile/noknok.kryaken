//ПЕРЕВОД ЦВЕТОВ
function toHEX(r, g, b) {
  return '#' + ((b | g << 8 | r << 16) | 1 << 24).toString(16).slice(1);
}
function getHexRGBColor(color)
    {
      color = color.replace(/\s/g,"");
      var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
     
      if(aRGB)

      {

        color = '';

        for (var i=1;  i<=3; i++) color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i])).toString(16).replace(/^(.)$/,'0$1');

      }

      else color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
 
      return color;
    }
/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
  r /= 255, g /= 255, b /= 255;
  var max = Math.max(r, g, b),
    min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;

  if (max == min) {
    h = s = 0; // achromatic
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h /= 6;
  }

  return [h, s, l];
}


function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    var hue2rgb = function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1 / 6) return p + (q - p) * 6 * t;
      if (t < 1 / 2) return q;
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}
  //

var opac = '0.3';
//ПЕРЕВОД ЦВЕТОВ


$(document).ready(function(){

function parseColor(color) {
    var arr=[]; color.replace(/[\d+\.]+/g, function(v) { arr.push(parseFloat(v)); });
    return {
        hex: "#" + arr.slice(0, 3).map(toHex).join(""),
        opacity: arr.length == 4 ? arr[3] : 1
    };
}
function toHex(int) {
    var hex = int.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

$('.pick_ajax').click(function(){
var pick_block = $(this).css('background-color');
var ncolor = parseColor(pick_block).hex;
//start перевод цвета для ховера
var c = getHexRGBColor(pick_block);

    //HEX to RGB
    var r = parseInt(c.substring(0, 2), 16);
    var g = parseInt(c.substring(2, 4), 16);
    var b = parseInt(c.substring(4, 6), 16);

    //RGB to HSL
    var hsl = rgbToHsl(r, g, b);


    var h = parseFloat(hsl[0]);
    var s = parseFloat(hsl[1]);
    var l = parseFloat(hsl[2]);
  mrgb = hslToRgb(h, s, l);
  var source = 'rgb('+ mrgb + ')';
  var opac_col = source.replace('rgb', 'rgba').replace(')', ',' + opac + ')');
    //set second color
    l = l + 0.20;
    s = s - 0.04;
    h = h + 0;

    //HSL to RGB
    rgb = hslToRgb(h, s, l);

    //RGB to HEX
    rgb = toHEX(rgb[0], rgb[1], rgb[2]);
//end перевод цвета для ховера

    $.ajax({
      type: "POST",
      url: "/bitrix/templates/noknok_kryaken/module-setings.php", //Change
      data: 'color='+ncolor + '&hover='+rgb + '&opacity='+opac_col,
    }).done(function() {
location.reload();
    });
    return false;



});

mc = parseColor($('.hero-btn').css('background-color')).hex;
$(".colorswitch").spectrum({

    color: mc,
    containerClassName: 'sp-bg',
    showInitial: true,
    chooseText: "Выбрать",
    cancelText: "Отмена",
    showInput: true,
    preferredFormat: "hex",
    change: function(color) {
//start перевод цвета для ховера
var col = color.toHexString();
var c = color.toHexString();

    //HEX to RGB
    var r = parseInt(c.substring(1, 3), 16);
    var g = parseInt(c.substring(3, 5), 16);
    var b = parseInt(c.substring(5, 7), 16);

    //RGB to HSL
    var hsl = rgbToHsl(r, g, b);


    var h = parseFloat(hsl[0]);
    var s = parseFloat(hsl[1]);
    var l = parseFloat(hsl[2]);
  mrgb = hslToRgb(h, s, l);
  var source = 'rgb('+ mrgb + ')';
  var opac_col = source.replace('rgb', 'rgba').replace(')', ',' + opac + ')');
    //set second color
    l = l + 0.20;
    s = s - 0.04;
    h = h + 0;

    //HSL to RGB
     rgb = hslToRgb(h, s, l);

    //RGB to HEX
     rgb = toHEX(rgb[0], rgb[1], rgb[2]);
//end перевод цвета для ховера
//цвет с прозрачностью

//

    $.ajax({
      type: "POST",
      url: "/bitrix/templates/noknok_kryaken/module-setings.php", //Change
      data: 'color='+col + '&hover='+rgb + '&opacity='+opac_col,
    }).done(function() {
location.reload();
    });
    return false;

    }
});


$('.switch-header-one').click(function(){

    $.ajax({
      type: "POST",
      url: "/bitrix/templates/noknok_kryaken/module-setings.php", 
      data: 'header='+'0',
    }).done(function() {
location.reload();
    });
    return false;

});
$('.switch-header-two').click(function(){

    $.ajax({
      type: "POST",
      url: "/bitrix/templates/noknok_kryaken/module-setings.php", 
      data: 'header='+'1',
    }).done(function() {
location.reload();
    });
    return false;

});


});