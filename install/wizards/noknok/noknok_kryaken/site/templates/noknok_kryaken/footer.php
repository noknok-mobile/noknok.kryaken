<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>
  <!-- START FOOTER MAP BOX -->
<?php if (!defined("ERROR_404")):?>
    <footer>
      <div class="container">
        <div class="footer-wrapper d-flex">
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-left">
              <div class="footer-logo d-flex">

                <div class="footer-logo-img">
					<?if(IsModuleInstalled($MODULE_ID)):?>
						<?$footer_logo = COption::GetOptionString($MODULE_ID, "footer_logo", "/bitrix/images/".$MODULE_ID."/footer_logo.png");?>
						<img src="<?=$footer_logo;?>" alt="">
					<?else:?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/footer_logo.php"
							)
						);?>
					<?endif;?>
                </div>
				<div class="footer-logo-text">
					<?if(IsModuleInstalled($MODULE_ID)):?>
						<?$slogan = COption::GetOptionString($MODULE_ID, "slogan", "KRYAKEN");?>
						<?echo $slogan;?>
					<?else:?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_DIR."include/logo_label.php"
							)
						);?>
					<?endif;?>
                </div>     
              </div>       
            </div>
          </div>
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-center d-flex">
              <div class="footer-social d-flex">
				<?if(IsModuleInstalled($MODULE_ID)):?>
					<?$vk_link = COption::GetOptionString($MODULE_ID, "vk_link", "https://vk.com/");?>
					<?$fb_link = COption::GetOptionString($MODULE_ID, "fb_link", "https://www.facebook.com/");?>
					<?$tw_link = COption::GetOptionString($MODULE_ID, "tw_link", "https://twitter.com/");?>
					<?$inst_link = COption::GetOptionString($MODULE_ID, "inst_link", "https://www.instagram.com/");?>
					<?$ytb_link = COption::GetOptionString($MODULE_ID, "ytb_link", "https://www.youtube.com/");?>
					<?$ok_link = COption::GetOptionString($MODULE_ID, "ok_link", "https://ok.ru/");?>
					<a href="<?=$vk_link;?>" target="_blank" class="footer-social-link vk"></a>
					<a href="<?=$fb_link;?>" target="_blank" class="footer-social-link fb"></a>
					<a href="<?=$tw_link;?>" target="_blank" class="footer-social-link tw"></a>
					<a href="<?=$inst_link;?>" target="_blank" class="footer-social-link in"></a>
					<a href="<?=$ytb_link;?>" target="_blank" class="footer-social-link yt"></a>
					<a href="<?=$ok_link;?>" target="_blank" class="footer-social-link ok"></a>
				<?else:?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_DIR."include/footer_social_links.php"
						)
					);?>
				<?endif;?>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-right">
              <div class="footer-payment d-flex">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_DIR."include/payment_links.php"
						)
					);?>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom-line-wr d-flex">
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-bottom-line-left d-flex">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_DIR."include/copyright.php"
						)
					);?>
            </div>
          </div>
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-bottom-line-center d-flex">
              <a href="#!" data-toggle="modal" data-target="#ps">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_DIR."include/politika_footer_label.php"
					)
				);?>
			</a>
            </div>
          </div>
          <div class="col-md-12 col-xl-4 col-lg-4">
            <div class="footer-bottom-line-right d-flex">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_DIR."include/payment_footer_label.php"
					)
				);?>
            </div>
          </div>
          
        </div>
      </div>
    </footer>
<?endif;?>
    <!-- END FOOTER MAP BOX -->
<!--start ќЅ–ј“Ќџ… «¬ќЌќ -->
<!-- Modal phone -->
<div class="modal fade" id="modalPhone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-cl-box">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      </div>
        <div class="modal-phone-title-box">
          <p class="modal-phone-title"><?=GetMessage("FEEDBACK_FORM_TITLE");?></p>
          <p class="modal-phone-untitle"><?=GetMessage("FEEDBACK_FORM_DESCRIPTION");?></p>
        </div>
        <div class="modal-phone-form-box">
          <form>
            <div class="form-group">
              <input type="text" class="form-control name" name="name" placeholder="<?=GetMessage("FEEDBACK_FORM_NAME_PLACEHOLDER");?>" required>
            </div>
            <div class="form-group">
              <input type="tel" class="form-control tel" name="phone" placeholder="+7 (999) 999-99-99" required>
            </div>
            <button type="submit" class="btn btn-primary btn-f bg"><?=GetMessage("FEEDBACK_FORM_BUTTON");?></button>
          </form>
        </div>
        <div class="politic-box">
          <span>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/politika_form_label.php"
				)
		);?>
		</span>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end ќЅ–ј“Ќџ… «¬ќЌќ -->

		<!--start  модальное окно —ѕј—»Ѕќ-->
            <div class="modal modal-ty fade" id="ty" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="modal-cl-box">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <div class="modal-ty-wrapper">
                      <div class="modal-ty-title">
                        <?=GetMessage("FEEDBACK_FORM_THANKS_TITLE");?>
                      </div>
                      <div class="modal-ty-text">
                        <p><?=GetMessage("FEEDBACK_FORM_THANKS_DESCRIPTION");?></p>
                      </div>
                      <div class="modal-ty-img d-flex">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/738.jpg" alt="">
                      </div>
                      <div class="modal-ty-footer">
                        <p><?=GetMessage("FEEDBACK_FORM_THANKS_BOTTOM_LABEL_FIRST");?></p>
                        <p><?=GetMessage("FEEDBACK_FORM_THANKS_BOTTOM_LABEL_SECOND");?></p>
                      </div>
                      <div class="modal-ty-footer-soc d-flex">
                        <div class="footer-social d-flex">
							<?if(IsModuleInstalled($MODULE_ID)):?>
								<?$vk_link = COption::GetOptionString($MODULE_ID, "vk_link", "https://vk.com/");?>
								<?$fb_link = COption::GetOptionString($MODULE_ID, "fb_link", "https://www.facebook.com/");?>
								<?$tw_link = COption::GetOptionString($MODULE_ID, "tw_link", "https://twitter.com/");?>
								<?$inst_link = COption::GetOptionString($MODULE_ID, "inst_link", "https://www.instagram.com/");?>
								<?$ytb_link = COption::GetOptionString($MODULE_ID, "ytb_link", "https://www.youtube.com/");?>
								<?$ok_link = COption::GetOptionString($MODULE_ID, "ok_link", "https://ok.ru/");?>
								<a href="<?=$vk_link;?>" target="_blank" class="footer-social-link vk"></a>
								<a href="<?=$fb_link;?>" target="_blank" class="footer-social-link fb"></a>
								<a href="<?=$tw_link;?>" target="_blank" class="footer-social-link tw"></a>
								<a href="<?=$inst_link;?>" target="_blank" class="footer-social-link in"></a>
								<a href="<?=$ytb_link;?>" target="_blank" class="footer-social-link yt"></a>
								<a href="<?=$ok_link;?>" target="_blank" class="footer-social-link ok"></a>
							<?else:?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE" => "",
										"PATH" => SITE_DIR."include/footer_social_links.php"
									)
								);?>
							<?endif;?>
                        </div>
                      </div>
                    </div>
  
                  </div>
                </div>
              </div>
            </div>
            <!--end модальное окно —ѕј—»Ѕќ-->

		<!--start соглашение модальное окно-->
            <div class="modal modal-ps fade" id="ps" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="modal-cl-box">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="ps-modal-box-wrapper">
						<?if(IsModuleInstalled($MODULE_ID)):?>
							<?$consent = COption::GetOptionString($MODULE_ID, "consent", GetMessage("CONSENT_TEXT"));?>
							<?echo $consent;?>
						<?else:?>
							<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE" => "",
										"PATH" => SITE_DIR."include/agreement.php"
									)
							);?>
						<?endif;?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--end соглашение модальное окно-->
			<!-- Bootstrap core JavaScript
			================================================== -->
			<!-- Placed at the end of the document so the pages load faster -->
			<script src="<?=SITE_TEMPLATE_PATH?>/colorpicker/spectrum.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/colorpicker/color-pick.js"></script>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
			<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.filterizr.min.js"></script>
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fltrnz.js"></script>

			<!--<script src="colorpicker/colorlight.js"></script>-->
			<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.min.js"></script>
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/js.js"></script>
			<?php if (defined("ERROR_404")):?>
				<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/404.js"></script>
			<?endif;?>
  	</body>
</html>