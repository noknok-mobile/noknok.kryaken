<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUADE_ID;?>">
	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<?$APPLICATION->AddHeadString("<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");?>
		<?$APPLICATION->AddHeadString("<meta name='description' content=''>");?>
		<?$APPLICATION->AddHeadString("<meta name='author' content=''>");?>
		<?$APPLICATION->AddHeadString("<link rel='shortcut icon' href='".SITE_DIR."favicon.ico' type='image/x-icon'>");?>
		<!-- Bootstrap core CSS -->
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/bootstrap.min.css');?>
		<!-- Custom styles for this template -->
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/jquery.fancybox.min.css');?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/colorpicker/spectrum.css');?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-3.3.1.min.js');?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick.min.js');?>
		<?$APPLICATION->ShowHead();?>
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<?
		$MODULE_ID = "noknok.kryaken";
		?>
		<?
			if(IsModuleInstalled($MODULE_ID))
			{
				$include_up = COption::GetOptionString($MODULE_ID, "include_up","N");
				if($include_up == "Y" && $USER->IsAdmin())
				{
					require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/noknok_settings.php');
				}
			}
		?>
<div id="menu_overlay"></div>
<div class="menu-box-wrapper">
  <div class="menu-box d-flex">
    <div class="close-btn-box d-flex">
      <span class="close-menu-btn"></span>
    </div>
    <div class="menu-list-box">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"hidden_menu",
			Array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "left",
				"COMPONENT_TEMPLATE" => "vertical_multilevel",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => "",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "Y"
			)
		);?>
    </div>
    <div class="menu-bottom-box">
      <div class="menu-phone">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/phone1.php"
				)
			);?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_DIR."include/phone2.php"
				)
			);?>
      </div>
      <div class="menu-call-btn-box">
        <button class="hero-btn bg" type="button" tabindex="0" data-toggle="modal" data-target="#modalPhone"><?=GetMessage("CALL_FEEDBACK");?></button>
      </div>
      <div class="menu-social-box">
        <div class="menu-social d-flex">
			<?if(IsModuleInstalled($MODULE_ID)):?>
				<?$vk_link = COption::GetOptionString($MODULE_ID, "vk_link", "https://vk.com/");?>
				<?$fb_link = COption::GetOptionString($MODULE_ID, "fb_link", "https://www.facebook.com/");?>
				<?$tw_link = COption::GetOptionString($MODULE_ID, "tw_link", "https://twitter.com/");?>
				<?$inst_link = COption::GetOptionString($MODULE_ID, "inst_link", "https://www.instagram.com/");?>
				<?$ytb_link = COption::GetOptionString($MODULE_ID, "ytb_link", "https://www.youtube.com/");?>
				<?$ok_link = COption::GetOptionString($MODULE_ID, "ok_link", "https://ok.ru/");?>
				<a href="<?=$vk_link;?>" target="_blank" class="menu-social-link vk"></a>
				<a href="<?=$fb_link;?>" target="_blank" class="menu-social-link fb"></a>
				<a href="<?=$tw_link;?>" target="_blank" class="menu-social-link tw"></a>
				<a href="<?=$inst_link;?>" target="_blank" class="menu-social-link in"></a>
				<a href="<?=$ytb_link;?>" target="_blank" class="menu-social-link yt"></a>
				<a href="<?=$ok_link;?>" target="_blank" class="menu-social-link ok"></a>
			<?else:?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_DIR."include/social_links.php"
					)
				);?>
			<?endif;?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if (!defined("ERROR_404")):?>
	<?if(IsModuleInstalled($MODULE_ID)):?>
		<?$header = COption::GetOptionString($MODULE_ID, "header", 1);?>
		<?if($header==0):?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/template_header_0.php');?>
		<?endif;?>
		<?if($header==1):?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/template_header_1.php');?>
		<?endif;?>
	<?else:?>
		<?require_once($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/template_header_1.php');?>
	<?endif;?>
<?endif;?>
