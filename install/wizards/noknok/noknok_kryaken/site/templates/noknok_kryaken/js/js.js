$(document).ready(function(){
$('.modal-catalog-slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.modal-catalog-slider-nav',
  arrows: false,

});
$('.modal-catalog-slider-nav').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.modal-catalog-slider',
  arrows: false,

});
//отправка формы
//форма обратной связи
$("#modalPhone form").submit(function() {
	var th = $(this);
	var title = $(this).closest('.modal-content').find('.modal-phone-title').text();
	 //Change
		$.ajax({
			type: "POST",
			url: "/bitrix/templates/noknok_kryaken/mail.php", //Change
			data: th.serialize() + '&form='+title,
		}).done(function() {

$('.modal').modal('hide');
setTimeout(function() { $('#ty').modal('show'); }, 700);

			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
});

//форма каталог
$(".modal-service .m-c-form").submit(function() {
	var th = $(this);
	var title = $(this).closest('.modal-content').find('.service-modal-title').text();
	 //Change
		$.ajax({
			type: "POST",
			url: "/bitrix/templates/noknok_kryaken/mail.php", //Change
			data: th.serialize() + '&form='+title,
		}).done(function() {

$('.modal').modal('hide');
setTimeout(function() { $('#ty').modal('show'); }, 700);

			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
});

//форма каталог
$(".modal-catalog .m-c-form").submit(function() {
	var th = $(this);
	var title = $(this).closest('.modal-content').find('.modal-catalog-desc-title').text();
	 //Change
		$.ajax({
			type: "POST",
			url: "/bitrix/templates/noknok_kryaken/mail.php", //Change
			data: th.serialize() + '&form='+title,
		}).done(function() {

$('.modal').modal('hide');
setTimeout(function() { $('#ty').modal('show'); }, 700);

			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
});
//<--отправка формы


$('.switch').click(function(){

$('.pick').toggleClass('visible');

});


if ($(window).width() > 768) {
  $('.hero-slide').height($(window).height());
} else {
$('.hero-slide').height(765);
}


$('.catalog-box-f span').click(function(){

$(".catalog-box-f span").removeClass('active');
$(this).addClass('active');

});

$('.gallery-box-f span').click(function(){

$(".gallery-box-f span").removeClass('active');
$(this).addClass('active');

});
//ЦВЕТ ЗНАЧКА КАРТЫ
color = $('.bg').css('background-color');

$('.comment-box-slider').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  prevArrow: '.comment-box-nav-left',
  nextArrow: '.comment-box-nav-right',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});




$('.modal-catalog').on('shown.bs.modal', function (e) {
$('.modal-catalog-slider').slick('unslick');
$('.modal-catalog-slider-nav').slick('unslick');
  $('.modal-catalog-slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.modal-catalog-slider-nav',
  arrows: false,

});


var sllength = $(this).find('.modal-catalog-slide-nav .modal-catalog-slide-nav-in').length;
	if (sllength > 3) {
$('.modal-catalog-slider-nav').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.modal-catalog-slider',
  arrows: false,

});
	} else {
$('.modal-catalog-slider-nav').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.modal-catalog-slider',
  arrows: false,

});
}

});

if ($(this).scrollTop() > 1){  
    $('header').addClass("fixed-on");
  }
  else{
    $('header').removeClass("fixed-on");
  }

$('.catalog-div-btn').click(function(){
  setTimeout(function() {
$('.modal-catalog-slider').slick('setPosition');
$('.modal-catalog-slider-nav').slick('setPosition');
  }, 700);
});


});

$(window).scroll(function() {

if ($(this).scrollTop() > 1){  
    $('header').addClass("fixed-on");
  }
  else{
    $('header').removeClass("fixed-on");
  }
});

$(window).resize(function() {
     
if ($(window).width() > 768) {
  $('.hero-slide').height($(window).height());
} else {
$('.hero-slide').height(765);
}
      
});



    var $menuRevealBtn = $('.head-btn-menu-box');
    var $sideNav = $('.menu-box-wrapper');
    var $sideNavMask = $('#menu_overlay');

    
    $menuRevealBtn.on('click', function() {
      $sideNav.addClass('visible');
      $sideNavMask.addClass('visible');
      $('.head').addClass('blur');
      $('.hero-box').addClass('blur');
      $('.description-box-wrapper').addClass('blur');
      $('.service-wrapper-box').addClass('blur');
      $('.steps-box-wrapper').addClass('blur');
      $('.goods-box-wrapper').addClass('blur');
      $('.catalog-box-wrapper').addClass('blur');
      $('.comment-box-wrapper').addClass('blur');
      $('.gallery-box-wrapper').addClass('blur');
      $('.map-box-wrapper').addClass('blur');
      $('footer').addClass('blur');
      $('body').addClass('modal-open');
      
    });
    
    $sideNavMask.on('click', function() {
      $sideNav.removeClass('visible');
      $sideNavMask.removeClass('visible');
      $('.head').removeClass('blur');
      $('.hero-box').removeClass('blur');
      $('.description-box-wrapper').removeClass('blur');
      $('.service-wrapper-box').removeClass('blur');
      $('.steps-box-wrapper').removeClass('blur');
      $('.goods-box-wrapper').removeClass('blur');
      $('.catalog-box-wrapper').removeClass('blur');
      $('.comment-box-wrapper').removeClass('blur');
      $('.gallery-box-wrapper').removeClass('blur');
      $('.map-box-wrapper').removeClass('blur');
      $('footer').removeClass('blur');
      $('body').removeClass('modal-open');
    });
    $('.close-menu-btn').on('click', function() {
      $sideNav.removeClass('visible');
      $sideNavMask.removeClass('visible');
      $('.head').removeClass('blur');
      $('.hero-box').removeClass('blur');
      $('.description-box-wrapper').removeClass('blur');
      $('.service-wrapper-box').removeClass('blur');
      $('.steps-box-wrapper').removeClass('blur');
      $('.goods-box-wrapper').removeClass('blur');
      $('.catalog-box-wrapper').removeClass('blur');
      $('.comment-box-wrapper').removeClass('blur');
      $('.gallery-box-wrapper').removeClass('blur');
      $('.map-box-wrapper').removeClass('blur');
      $('footer').removeClass('blur');
      $('body').removeClass('modal-open');
    });
	$('.menu-box .menu-list li a').on('click', function() {
      $sideNav.removeClass('visible');
      $sideNavMask.removeClass('visible');
      $('.head').removeClass('blur');
      $('.hero-box').removeClass('blur');
      $('.description-box-wrapper').removeClass('blur');
      $('.service-wrapper-box').removeClass('blur');
      $('.steps-box-wrapper').removeClass('blur');
      $('.goods-box-wrapper').removeClass('blur');
      $('.catalog-box-wrapper').removeClass('blur');
      $('.comment-box-wrapper').removeClass('blur');
      $('.gallery-box-wrapper').removeClass('blur');
      $('.map-box-wrapper').removeClass('blur');
      $('footer').removeClass('blur');
      $('body').removeClass('modal-open');
    });



function color_header() {
var div = document.getElementById('nnn');

if( div.classList.contains('fixed-on'))
  {
    $('#nl').fadeOut( 300, function() {
    $('#fl').fadeIn(200);
  });
  } else {
    $('#fl').fadeOut(50, function() {
    $('#nl').fadeIn(50);
  });
  }
}

timerId = setInterval(color_header, 1);



if($("div").is("#bx-panel"))
{
function margin_header() {
var bp_height = $('#panel').height();
	$('header').offset({top: bp_height});
}
timerId = setInterval(margin_header, 50);
}


$(document).ready(function(){
    $("header a").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

if($("div").is("#bx-panel"))
{
$('.head-right').find('ul').parent().addClass('fl-one');
}

$('.head-logo').click(function(){
location.reload();
});

$('.head-logo-text').click(function(){
location.reload();
});

});