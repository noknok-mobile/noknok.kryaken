$(document).ready(function(){


  ymaps.ready(init);

function init () {

  var mymap = new ymaps.Map('map', {

    center: [47.21821607, 39.71055700],
    zoom: 16,
    controls: [],
    behaviors: ['drag']

  });
  MyIconLayout = ymaps.templateLayoutFactory.createClass([
  	'<div class="icon-map">',
            '<svg xmlns="http://www.w3.org/2000/svg" width="38" height="51">',
                '<path fill="' + color + '" fill-rule="evenodd" d="M19 0C8.523 0 0 8.281 0 18.46c0 12.632 17.003 31.176 17.727 31.96.68.735 1.867.734 2.546 0C20.997 49.636 38 31.092 38 18.46 38 8.281 29.477 0 19 0zm0 27.748c-5.271 0-9.559-4.167-9.559-9.288 0-5.122 4.288-9.288 9.559-9.288 5.271 0 9.559 4.167 9.559 9.288S24.271 27.748 19 27.748z"/>',
            '</svg>',
    '</div>'
        ].join('')),
  myPlacemark = new ymaps.Placemark(mymap.getCenter(), {
            hintContent: 'Noknok',
            balloonContent: 'г. Ростов-на-Дону, ул. Темерницкая 44, оф. 406'
        }, {

            iconLayout: MyIconLayout,
            iconShape: {
                type: 'Rectangle',
                coordinates: [
                    [-19, -55], [19, 0]
                ]
            }
            
            
        });
  mymap.geoObjects.add(myPlacemark);

}

});