$(window).on("load", function (e) {
  $('.filtr-container').filterizr({
  controlsSelector: '.ctlg-fltr',
   setupControls: false, 
   animationDuration: .45,
   delay: 22,
   delayMode: 'progressive', 
   filter: 'all',
   layout: 'sameSize',
   easing: 'ease-out',
   filterOutCss: { 
      opacity: 0,
      transform: 'scale(0.6)'
   }, 
   
   filterInCss: { 
      opacity: 1,
      transform: 'scale(1)'
   }

});


$('.filtr-gallery').filterizr({
  controlsSelector: '.gal-fltr',
   setupControls: false, 
   animationDuration: .45,
   delay: 22,
   delayMode: 'progressive', 
   filter: 'all',
   layout: 'sameSize',
   easing: 'ease-out',
   filterOutCss: { 
      opacity: 0,
      transform: 'scale(0.6)'
   }, 
   
   filterInCss: { 
      opacity: 1,
      transform: 'scale(1)'
   }

});
});