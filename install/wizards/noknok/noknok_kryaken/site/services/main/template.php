<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (!defined("WIZARD_TEMPLATE_ID"))
    return;
$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . WIZARD_TEMPLATE_ID;
CopyDirFiles(
    $_SERVER["DOCUMENT_ROOT"] . WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH . "/site") . "/" . WIZARD_TEMPLATE_ID,
    $bitrixTemplateDir,
    $rewrite = true,
    $recursive = true,
    $delete_after_copy = false
);

//Attach template to default site
$obSite = CSite::GetList($by = "def", $order = "desc", Array("LID" => WIZARD_SITE_ID));
if ($arSite = $obSite->Fetch()) {
    $arTemplates = Array();
    $arTemplates[] = Array("CONDITION" => "", "SORT" => 1, "TEMPLATE" => "noknok_kryaken");
    $arFields = Array(
        "TEMPLATE" => $arTemplates,
        "NAME" => $arSite["NAME"],
    );
    $obSite = new CSite();
    $obSite->Update($arSite["LID"], $arFields);
}

$obEventType = new CEventType;
$obEventType->Add(array(
    "EVENT_NAME"    => "NOKNOK_FEEDBACK_FORM",
    "NAME"          => GetMessage("NAME1"),
    "LID"           => "ru",
    "DESCRIPTION"   => GetMessage("DES1")
    ));
    
	
$obEventType2 = new CEventType;
$obEventType2->Add(array(
    "EVENT_NAME"    => "NOKNOK_SERVICES_FEEDBACK",
    "NAME"          => GetMessage("NAME2"),
    "LID"           => "ru",
    "DESCRIPTION"   => GetMessage("DES2")
    ));
	
    $obTemplate = new CEventMessage;
    $obTemplate->Add(array(
        "ACTIVE" => "Y",
        "EVENT_NAME" => "NOKNOK_FEEDBACK_FORM",
        "LID" => "s1",
        "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
        "EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
        "BCC" => "#BCC#",
        "SUBJECT" =>  GetMessage('SUBJECT1'),
        "MESSAGE" => GetMessage('STRING1'),
        "BODY_TYPE" => "html",
    ));
	
	$obTemplate2 = new CEventMessage;
    $obTemplate2->Add(array(
        "ACTIVE" => "Y",
        "EVENT_NAME" => "NOKNOK_SERVICES_FEEDBACK",
        "LID" => "s1",
        "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
        "EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
        "BCC" => "#BCC#",
        "SUBJECT" =>  GetMessage('SUBJECT2'),
        "MESSAGE" => GetMessage('STRING2'),
        "BODY_TYPE" => "html",
    ));

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("SITE_TITLE" => htmlspecialcharsbx($wizard->GetVar("siteName"))));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_TITLE" => htmlspecialcharsbx($wizard->GetVar("siteName"))));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/.section.php", array("SITE_DESCRIPTION" => htmlspecialcharsbx($wizard->GetVar("siteDescription"))));
COption::SetOptionString("main", "wizard_template_id", WIZARD_TEMPLATE_ID, false, WIZARD_SITE_ID);
?>