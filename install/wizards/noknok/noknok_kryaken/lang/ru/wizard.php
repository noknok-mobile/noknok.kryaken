<?
$MESS["FINISH_STEP_TITLE"] = "Установка решения завершена";
$MESS["wiz_go"] = "Далее";
$MESS["wiz_site_name"] = "NOKNOK: KRYAKEN - Адаптивный лендинг";
$MESS["FINISH_STEP_CONTENT"] = "Сайт нашей компании: <a href=\"https://noknok.ru\">NOKNOK.RU</>";
$MESS["FINISH_STEP_REINDEX"] = "????";
$MESS["wiz_name"] = "Адаптивный лендинг для строительной фирмы, агентства недвижимости, строительных товаров.";
$MESS["wiz_slogan"] = "Адаптивный лендинг для строительной фирмы, агентства недвижимости, строительных товаров.";
$MESS["wiz_company_name"] = "Название сайта:";
$MESS["wiz_company_description"] = "Описание сайта:";
$MESS["wisCopyright"] = "Разработано в &copy; NOKNOK.RU. All Rights Reserved";
?>