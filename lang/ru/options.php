<?
    $MESS ['INCLUDE_UP'] = "Показывать настройщик в публичной части сайта";
	$MESS ['COLOR_SETTINGS'] = "Настройки цвета";
	$MESS ['MAIN_COLOR'] = "Основной цвет";
	$MESS ['HOVER_COLOR'] = "Цвет ховера";
	$MESS ['OPACITY_COLOR'] = "Прозрачность";
	$MESS ['HEADER_SETTINGS'] = "Настройки шапки";
	$MESS ['HEADER'] = "Вид отображения шапки";
	$MESS ['HEADER_LEFT'] = "Скрытая боковая";
	$MESS ['HEADER_TOP'] = "С верхним меню";
	$MESS ['WHITE_LOGO'] = "Белое лого";
	$MESS ['OPACITY_LOGO'] = "Прозрачное лого";
	$MESS ['OPACITY_LOGO_COLOR'] = "Заливать цветом прозрачное лого";
	$MESS ['SLOGAN'] = "Слоган";
	$MESS ['FOOTER_SETTINGS'] = "Настройки футера";
	$MESS ['FOOTER_LOGO'] = "Лого в футере";
	$MESS ['CONSENT'] = "Текст согласия на обработку персональных данных";
	$MESS ['CONSENT_TEXT'] = "Согласен на обработку персональных данных";
	$MESS ['SOCIAL_LINKS_SETTINGS'] = "Настройки социальных сетей";
	$MESS ['VK_LINK'] = "Ссылка ВКонтакте";
	$MESS ['FB_LINK'] = "Ссылка Facebook";
	$MESS ['TW_LINK'] = "Ссылка Twitter";
	$MESS ['INST_LINK'] = "Ссылка Instagram";
	$MESS ['YTB_LINK'] = "Ссылка YouTube";
	$MESS ['OK_LINK'] = "Ссылка Одноклассники";
	$MESS ['BLOCK_VIEW_SETTINGS'] = "Настройки внешнего вида блоков";
	$MESS ['FIRST_BLOCK_VIEW'] = "Текстовый блок с картинкой";
	$MESS ['WHITE_VIEW'] = "С белым фоном";
	$MESS ['GRAY_VIEW'] = "С серым фоном";
	$MESS ['OFF_VIEW'] = "Выключен";
	$MESS ['SECOND_BLOCK_VIEW'] = "Блок услуг";
	$MESS ['THIRD_BLOCK_VIEW'] = "Блок с этапами работы";
	$MESS ['FOURTH_BLOCK_VIEW'] = "Блок с преимуществами";
	$MESS ['COLOR_VIEW'] = "С цветным фоном";
	$MESS ['IMAGE_VIEW'] = "С фоновой картинкой";
	$MESS ['FIVE_BLOCK_VIEW'] = "Блок с каталогом";
	$MESS ['SIX_BLOCK_VIEW'] = "Блок с отзывами";
	$MESS ['SEVEN_BLOCK_VIEW'] = "Блок с галереей";
	$MESS ['EIGHT_BLOCK_VIEW'] = "Блок с контактами";

    $MESS ['FILE_UPLOAD'] = "Загрузить картинку";
    $MESS ['MAIN_TAB_SET'] = "Настройки";
	$MESS ['MAIN_TAB_TITLE_SET'] = "Настройки готового решения NOKNOK:KRYAKEN - Адаптивный лендинг";
    $MESS ['FILE_UPLOAD_IMG'] = "Стандартная картинка";
    $MESS ['ERROR'] = 'Ошибка. Не верный формат. Загрузите изображение формата: ';
    $MESS ['TYPE_FIX'] = "<br /><b>Настройки для фиксированного блока:</b> ";
    $MESS['SAVE'] = "Сохранить";
    $MESS['APPLY'] = "Применить";
    $MESS['RESTORE_DEFAULTS'] = "По-умолчанию";
    $MESS['RESTORE_DEFAULTS_WARNING'] = "Внимание! Все настройки будут перезаписаны значениями по умолчанию! Продолжить?";
?>
