<?
	$MESS["MODULE_NAME"] = "NOKNOK:KRYAKEN - Адаптивный лендинг";
    $MESS["MODULE_DESCRIPTION"] = "Адаптивный лендинг для строительной фирмы, агентства недвижимости, строительных товаров";
    $MESS["MODULE_AUTHOR"] = "NOKNOK";
    $MESS["MODULE_URI"] = "https://noknok.ru/";
    $MESS["INSTALL_TITLE"] = "Установка модуля: NOKNOK::KRYAKEN";
    $MESS["UNINSTALL_TITLE"] = "Удаление модуля: NOKNOK::KRYAKEN";
    $MESS["INSTALL_ACCESS_DENIED"] = "Доступ запрещен";
    $MESS["INSTALL_FULL_ACCESS"] = "Полный доступ";
    $MESS["MOD_INST_ERR"] = "Ошибка установки";
    $MESS["MOD_INST_OK"] = "Модуль установлен";
?>
