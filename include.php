<?
Class noknok_kryaken_class {

    function blockAndUp() {
        global $APPLICATION;
        $MODULE_ID = "noknok.kryaken";
        if (IsModuleInstalled($MODULE_ID)) {
            if (!defined("ADMIN_SECTION") && $ADMIN_SECTION !== true) {
			$main_color = COption::GetOptionString($MODULE_ID, "main_color", "#0076e3");
			$hover_color = COption::GetOptionString($MODULE_ID, "hover_color", "rgb(78, 168, 251)");
			$opacity_color = COption::GetOptionString($MODULE_ID, "opacity_color", "rgba(0, 118, 227, .3)");
                if ($enable_jq == "Y") {
                    CUtil::InitJSCore(Array("jquery"));
                }
			$base_style = $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/color.css";
				$style_doc = "
					.head-phone-h-two a:hover {
						color: %color%;
					}
					.fixed-on .menu-header-two a:hover {
					color:  %color%;
					}
					.fixed-on .menu-header-two a {
					color:  %color%;
					}
					.bg_logo {
					background: %color%;
					}
					a {
						color: %color%;
					}
					a:hover {
						color: %color%;
					}
			
					.hero-btn {
					background-color: %color%;
					border: 1px solid %color%;
					}
					.btn-head-right:hover {
					background-color: %color%;
					border: 1px solid %color%;
					}
					.head-phone:hover {
					  color: %color%;
					}
					.head-phone:hover a {
					color: %color%;
					}
					.head-phone:hover span {
						border: solid 1px %color%;
					}
					.head-phone:hover svg path {
						fill: %color%;
					}
					.s-btn {
					  color: %color%;
					  border: 1px solid %color%;
					}
					.s-btn:hover {
					  background: %color%;
					}
					.service-box-link {
					border-bottom: 1px solid %opacity%;
					}
					.goods-box-wrapper {
					  background-color: %color%;
					}
					.catalog-div-btn {
					  color: %color%;
					  border: 1px solid %color%;
					}
					.catalog-div-btn:hover {
					  background-color: %color%;
					}
					.catalog-div:hover .catalog-div-price-new {
					color: %color%;
					}
					.catalog-box-f span:hover {
					  color: %color%;
					  border-bottom: 1px solid %opacity%;
					}
					.catalog-box-f span.active {
					color: %color%;
					border-bottom: 1px solid %opacity%;
					}
					.gallery-box-f span:hover {
					  color: %color%;
					   border-bottom: 1px solid %opacity%;
					}
					.gallery-box-f span.active {
					border-bottom: 1px solid %opacity%;
					color: %color%;
					}
					.map-box-wrapper {
					  background-color: %color%;
					}
					.comment-box-wrapper {
					  background-color: %color%;
					}
					.footer-bottom-line-center p {
					  color: %color%;
					}
					.close-menu-btn:hover:before, .close-menu-btn:hover:after {
					  background-color: %color%;
					}
					.head-btn-menu-box:hover svg path {
					fill: %color%;
					}
					.menu-list li a {
					  color: %color%;
					  border-bottom: 1px solid %opacity%;
					}
					.menu-phone a {
					color: %color%;
					}
					.btn-f {
					background-color: %color%;
					border-color: %color%;
					}
					.modal-price {
						color: %color%;
					}
					.politic-box a {
					color: %color%;
					}
					.icon-map svg path {
					  fill: %color%;
					}
					.comment-box-slide-file svg path {
					  fill: %color%;
					}
					.slick-dots li.slick-active button {
						background-color: %color%;
					}
					.head.fixed-on .head-btn-menu-box svg path {
						fill: %color%;
					}
					.head.fixed-on .head-phone a {
						color: %color%;
					}
			
					.head.fixed-on .head-phone a:hover {
						color: %hover%;
					}
					.head.fixed-on .head-phone:hover span {
					border: 1px solid %hover%;
					}
					.head.fixed-on .head-phone:hover svg path  {
					fill: %hover%;
					}
					.head.fixed-on .head-logo-text {
						color: %color%;
					}
					.head.fixed-on .head-phone svg path {
						fill: %color%;
					}
					.head.fixed-on .head-phone span {
						border: solid 1px %color%;
					}
					.head.fixed-on .btn-head-right {
						color: %color%;
						border: 1px solid %color%;
					}
					.footer-bottom-line-center a {
					border-bottom: 1px solid %opacity%;
					}
					.comment-box-slide-file a {
					border-bottom: 1px solid %opacity%;
					}
			
					.hero-btn:hover, .btn-f:hover {
					  background-color: %hover%;
					  border: 1px solid %hover%;
					}
					.head-phone:hover span {
					border: 1px solid %hover%;
					}
					.head-phone:hover svg path  {
					fill: %hover%;
					}
					.head-phone:hover a {
					color: %hover%;
					}
					//new header
					.head.fixed-on .menu-header-two a {
						color: %color%;
					}
					.head.fixed-on .head-phone-tab-tel a {
					  color: %color%;
					}
					.head.fixed-on .head-phone-h-two span {
						border: solid 1px %color%;
					}
					.head.fixed-on .head-phone-h-two span svg path {
						fill: %color%;
					}
					.btn-ff {
						background-color: %color%;
					}
					ul.mark-list li::before {
						background-color: %color%;
					}
					.site-color {
					background-color: %color% !important;
					}
				";
				$new_color = str_replace("%color%", $main_color, $style_doc);
				$new_hover = str_replace("%hover%", $hover_color, $new_color);
				$new_opacity = str_replace("%opacity%", $opacity_color, $new_hover);
			
				$fp = fopen ($base_style, "w");
				fwrite ($fp,$new_opacity);
				fclose ($fp);
				$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/color.css');
            }
        }
    }
}
?>