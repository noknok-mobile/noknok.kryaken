<?
IncludeModuleLangFile(__FILE__);

if (!$USER->IsAdmin())
    return;

$MODULE_ID = "noknok.kryaken";

$arAllOptions = array(
    "include_up" => array("include_up", GetMessage("INCLUDE_UP"), "N", array("checkbox","N")),
	GetMessage("COLOR_SETTINGS"),
	"main_color" => array("main_color", GetMessage("MAIN_COLOR"),"#0076e3", array("text", 30)),
	"hover_color" => array("hover_color", GetMessage("HOVER_COLOR"),"rgb(78, 168, 251)", array("text", 30)),
	"opacity_color" => array("opacity_color", GetMessage("OPACITY_COLOR"),"rgba(0, 118, 227, .3)", array("text", 30)),
	GetMessage("HEADER_SETTINGS"),
    "header" => array("header", GetMessage("HEADER"), 1, array("selectbox", array(GetMessage("HEADER_LEFT"), GetMessage("HEADER_TOP")))),
    "white_logo" => array("white_logo", GetMessage("WHITE_LOGO"), "/bitrix/images/".$MODULE_ID."/white_logo.png", "file"),
	"opacity_logo" => array("opacity_logo", GetMessage("OPACITY_LOGO"), "/bitrix/images/".$MODULE_ID."/opacity_logo.png", "file"),
    "opacity_logo_color" => array("opacity_logo_color", GetMessage("OPACITY_LOGO_COLOR"), "Y", array("checkbox","Y")),
	"slogan" => array("slogan", GetMessage("SLOGAN"),"KRYAKEN", array("text", 30)),
	GetMessage("FOOTER_SETTINGS"),
	"footer_logo" => array("footer_logo", GetMessage("FOOTER_LOGO"), "/bitrix/images/".$MODULE_ID."/footer_logo.png", "file"),
    "consent" => array("consent", GetMessage("CONSENT"), GetMessage("CONSENT_TEXT"), Array("textarea", 10,100)),
	GetMessage("SOCIAL_LINKS_SETTINGS"),
	"vk_link" => array("vk_link", GetMessage("VK_LINK"), "https://vk.com/", Array("text", 30)),
	"fb_link" => array("fb_link", GetMessage("FB_LINK"), "https://www.facebook.com/", Array("text", 30)),
	"tw_link" => array("tw_link", GetMessage("TW_LINK"), "https://twitter.com/", Array("text", 30)),
	"inst_link" => array("inst_link", GetMessage("INST_LINK"), "https://www.instagram.com/", Array("text", 30)),
	"ytb_link" => array("ytb_link", GetMessage("YTB_LINK"), "https://www.youtube.com/", Array("text", 30)),
	"ok_link" => array("ok_link", GetMessage("OK_LINK"), "https://ok.ru/", Array("text", 30)),
	GetMessage("BLOCK_VIEW_SETTINGS"),
    "first_block_view" => array("first_block_view", GetMessage("FIRST_BLOCK_VIEW"), "white_view", array("selectbox", array("white_view"=>GetMessage("WHITE_VIEW"), "gray_view"=>GetMessage("GRAY_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
    "second_block_view" => array("second_block_view", GetMessage("SECOND_BLOCK_VIEW"), "gray_view", array("selectbox", array("white_view"=>GetMessage("WHITE_VIEW"), "gray_view"=>GetMessage("GRAY_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"third_block_view" => array("third_block_view", GetMessage("THIRD_BLOCK_VIEW"), "white_view", array("selectbox", array("white_view"=>GetMessage("WHITE_VIEW"), "gray_view"=>GetMessage("GRAY_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"fourth_block_view" => array("fourth_block_view", GetMessage("FOURTH_BLOCK_VIEW"), "color_view", array("selectbox", array("color_view"=>GetMessage("COLOR_VIEW"), "image_view"=>GetMessage("IMAGE_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"five_block_view" => array("five_block_view", GetMessage("FIVE_BLOCK_VIEW"), "white_view", array("selectbox", array("white_view"=>GetMessage("WHITE_VIEW"), "gray_view"=>GetMessage("GRAY_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"six_block_view" => array("six_block_view", GetMessage("SIX_BLOCK_VIEW"), "color_view", array("selectbox", array("color_view"=>GetMessage("COLOR_VIEW"), "image_view"=>GetMessage("IMAGE_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"seven_block_view" => array("seven_block_view", GetMessage("SEVEN_BLOCK_VIEW"), "white_view", array("selectbox", array("white_view"=>GetMessage("WHITE_VIEW"), "gray_view"=>GetMessage("GRAY_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
	"eight_block_view" => array("eight_block_view", GetMessage("EIGHT_BLOCK_VIEW"), "color_view", array("selectbox", array("color_view"=>GetMessage("COLOR_VIEW"), "image_view"=>GetMessage("IMAGE_VIEW"), "off"=>GetMessage("OFF_VIEW")))),
);

$aTabs = array(
    array('DIV' => 'edit1', 'TAB' => GetMessage('MAIN_TAB_SET'), 'TITLE' => GetMessage('MAIN_TAB_TITLE_SET'), 'OPTIONS' => $arAllOptions)
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);
$tabControl->Begin();

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption($MODULE_ID);
    } else {
        foreach ($arAllOptions as $name => $arOption) {
            if ($name == 'white_logo') {
                if (strlen($_FILES['white_logo']["tmp_name"]) > 0) {
                    $img_type = preg_split("#\/#", $_FILES["white_logo"]["type"]);
                    if ($img_type[0] == 'image') {
                        $tmp_name = $_FILES["white_logo"]["tmp_name"];
                        $filename = $_FILES["white_logo"]["name"];

                        if (move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'] . "/bitrix/images/".$MODULE_ID."/" . $filename)) {
                             COption::SetOptionString($MODULE_ID, 'white_logo', "/bitrix/images/".$MODULE_ID."/".$filename);
                        }
                    } else {
                        CAdminMessage::ShowMessage(GetMessage("ERROR") . $img_type[0]);
                    }
                }
            }
            elseif ($name == 'opacity_logo') {
                if (strlen($_FILES['opacity_logo']["tmp_name"]) > 0) {
                    $img_type = preg_split("#\/#", $_FILES["opacity_logo"]["type"]);
                    if ($img_type[0] == 'image') {
                        $tmp_name = $_FILES["opacity_logo"]["tmp_name"];
                        $filename = $_FILES["opacity_logo"]["name"];

                        if (move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'] . "/bitrix/images/".$MODULE_ID."/" . $filename)) {
                             COption::SetOptionString($MODULE_ID, 'opacity_logo', "/bitrix/images/".$MODULE_ID."/".$filename);
                        }
                    } else {
                        CAdminMessage::ShowMessage(GetMessage("ERROR") . $img_type[0]);
                    }
                }
            }
            elseif ($name == 'footer_logo') {
                if (strlen($_FILES['footer_logo']["tmp_name"]) > 0) {
                    $img_type = preg_split("#\/#", $_FILES["footer_logo"]["type"]);
                    if ($img_type[0] == 'image') {
                        $tmp_name = $_FILES["footer_logo"]["tmp_name"];
                        $filename = $_FILES["footer_logo"]["name"];

                        if (move_uploaded_file($tmp_name, $_SERVER['DOCUMENT_ROOT'] . "/bitrix/images/".$MODULE_ID."/" . $filename)) {
                             COption::SetOptionString($MODULE_ID, 'footer_logo', "/bitrix/images/".$MODULE_ID."/".$filename);
                        }
                    } else {
                        CAdminMessage::ShowMessage(GetMessage("ERROR") . $img_type[0]);
                    }
                }
            }

			 else {
                $name = $arOption[0];
                $val = $_REQUEST[$name];
                if (isset($arOption[3]) && isset($arOption[3][0]) && $arOption[3][0] == "checkbox" && $val != "Y")
                    $val = "N";
                COption::SetOptionString($MODULE_ID, $name, $val, $arOption[1]);
            }
        }
    }

    if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}
?>

<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>" enctype="multipart/form-data">
<? $tabControl->BeginNextTab(); ?>
<?

foreach ($arAllOptions as $arOption) {
    if (count($arOption) == 1) { ?>
        <tr class="heading">
            <td colspan="2"><?=$arOption?></td>
        </tr>
    <? } else {
        $val = COption::GetOptionString($MODULE_ID, $arOption[0], $arOption[2]);
        $type = $arOption[3];
        ?>
            <tr>
                <td width="40%" nowrap <? if ($type[0] == "textarea") echo 'class="adm-detail-valign-top"' ?>>
                    <label for="<? echo htmlspecialcharsbx($arOption[0]) ?>"><? echo $arOption[1] ?>:</label>
                </td>
                <td width="60%">
            <? if ($type[0] == "checkbox"): ?>
                        <input type="checkbox" id="<? echo htmlspecialcharsbx($arOption[0]) ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>" value="Y"<? if ($val == "Y") echo" checked"; ?>>
            <? elseif ($type[0] == "text"): ?>
                        <input type="text" size="<? echo $type[1] ?>" maxlength="255" value="<? echo htmlspecialcharsbx($val) ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>">
            <? elseif ($type[0] == "textarea"): ?>
                        <textarea rows="<? echo $type[1] ?>" cols="<? echo $type[2] ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>"><? echo htmlspecialcharsbx($val) ?></textarea>
            <? elseif ($type[0] == "selectbox"): ?>
                        <select name="<? echo htmlspecialcharsbx($arOption[0]) ?>">
                            <? foreach ($type[1] as $t_key=>$t_val) { ?>
                                <option value="<?=htmlspecialcharsbx($t_key);?>" <?=(htmlspecialcharsbx($val) == htmlspecialcharsbx($t_key)) ? "selected" : "" ?>>
                                    <?=htmlspecialcharsbx($t_val);?>
                                </option>
                            <? } ?>
                        </select>
            <? endif ?>
            <? if ($type == "file" && $arOption[0]=="white_logo"): ?>
                    <img src="<?= COption::GetOptionString($MODULE_ID, "white_logo", "/bitrix/images/".$MODULE_ID."/white_logo.png") ?>">
                    <input type="file" name="white_logo"  />     
            <? endif ?>
            <? if ($type == "file" && $arOption[0]=="opacity_logo"): ?>
                    <img src="<?= COption::GetOptionString($MODULE_ID, "opacity_logo", "/bitrix/images/".$MODULE_ID."/opacity_logo.png") ?>">
                    <input type="file" name="opacity_logo"  />     
            <? endif ?>
            <? if ($type == "file" && $arOption[0]=="footer_logo"): ?>
                    <img src="<?= COption::GetOptionString($MODULE_ID, "footer_logo", "/bitrix/images/".$MODULE_ID."/footer_logo.png") ?>">
                    <input type="file" name="footer_logo"  />     
            <? endif ?>
                </td>
            </tr>
        <?
    }
}
?>
            <? $tabControl->Buttons(); ?>
    <input type="submit" name="Update" value="<?= GetMessage("SAVE") ?>" title="<?= GetMessage("SAVE") ?>" class="adm-btn-save">
    <input type="submit" name="Apply" value="<?= GetMessage("APPLY") ?>" title="<?= GetMessage("APPLY") ?>">
            <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
        <input type="button" name="Cancel" value="<?= GetMessage("CANCEL") ?>" title="<?= GetMessage("CANCEL") ?>" onclick="window.location = '<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
    <? endif ?>
    <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("RESTORE_DEFAULTS") ?>" OnClick="return confirm('<? echo AddSlashes(GetMessage("RESTORE_DEFAULTS_WARNING")) ?>')" value="<? echo GetMessage("RESTORE_DEFAULTS") ?>">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>